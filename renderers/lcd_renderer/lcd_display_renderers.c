/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "lcd_display_renderers.h"
#include "../../lcd/Adafruit_LCD.h"

Adafruit_LCD *init_tft_lcd(int lcd_width, int lcd_height) {

	Adafruit_LCD *lcd = malloc(sizeof(Adafruit_LCD));
	Adafruit_LCD_construct(lcd, lcd_width, lcd_height);

	if (readReg(0) != 0x75){
		while(1){
			printf("ERROR: Board undetected\n");
			readReg(0);
			delay(10);
		}
	}

	initialize(lcd);
	displayOn(true);

	/* Enable TFT - display enable tied to GPIOX */
	GPIOX(true);
	/* PWM output for backlight */
	PWM1config(true, RA8875_PWM_CLK_DIV1024);
	/* Max brightness */
	PWM1out(255);

	/* Startup Display */
	graphicsMode();
	fillScreen(lcd, RA8875_BLACK);
	drawNotificationBorder(lcd);
	textMode();
	textSetCursor(130,140);

	/* Display project and group name */
	char string[18] = "Tabletop Notifier";
	textEnlarge(lcd,3);
	textColor(RA8875_CYAN, RA8875_BLACK);
	textWrite(lcd, string, 18);

	textSetCursor(275,230);
	textEnlarge(lcd,3);
	textColor(RA8875_CYAN, RA8875_BLACK);
	textWrite(lcd, "Group 6", 8);
	delay(5000);

	/* Fade out (lower screen brightness) */
	alt_u8 i;
	for (i=255 ; i!=0 ; i-=5 ){
		PWM1out(i);
		delay(10);
	}

	return lcd;
}

void lcd_display_time_renderer(struct time_ui_data *time_data, Adafruit_LCD *lcd) {

	/* Clear and fill screen */
	graphicsMode();
	fillScreen(lcd, RA8875_BLACK);
	drawScreenBorder(lcd);

	// Fade in (increase screen brightness)
	alt_u8 i;
	for (i=0; i!=255; i+=5 ){
		PWM1out(i);
		delay(10);
	}
	PWM1out(255);

	/* Write the time to LCD*/
	textMode();
	textSetCursor(300,100);
	textColor(RA8875_WHITE, RA8875_BLACK);
	textEnlarge(lcd,3);
	textWrite(lcd, time_data->time_str, TIME_BUFF_SIZE);

	/* Write the notification count below time*/
	textSetCursor(220,240);
	textColor(RA8875_WHITE, RA8875_BLACK);
	textEnlarge(lcd,2);
	textWrite(lcd, time_data->notifs_str, NOTIF_CNT_BUFF_SIZE);

}

void lcd_display_new_notif_renderer(struct notif_ui_data *notif_data, Adafruit_LCD *lcd) {

	/* Clear and fill screen */
	graphicsMode();
	fillScreen(lcd, RA8875_BLACK);
	drawNotificationBorder(lcd);
	textMode();

	// Fade in (increase screen brightness)
	alt_u8 i;
	for (i=0; i!=255; i+=5 ){
		PWM1out(i);
		delay(10);
	}
	PWM1out(255);

	/* Write app info to LCD*/
	textSetCursor(40, 43);
	textEnlarge(lcd,2);
	textColor(RA8875_WHITE, RA8875_BLACK);
	textWrite(lcd, notif_data->notif->app, NOTIF_APP_SIZE);

	/* Write title info to LCD*/
	textSetCursor(40,100);
	textEnlarge(lcd,2);
	textColor(RA8875_WHITE, RA8875_BLACK);
	textWrite(lcd, notif_data->notif->title, NOTIF_TITLE_SIZE);
	OSTimeDly(OS_TICKS_PER_SEC);

	/* Write notification content */
	textSetCursor(40, 170);

	textEnlarge(lcd,1);
	textColor(RA8875_WHITE, RA8875_BLACK);
	textWrite(lcd, notif_data->notif->text, NOTIF_TEXT_SIZE);

	/* Display for 5 seconds */
	OSTimeDly(OS_TICKS_PER_SEC*5);

	/* Fade out (lower screen brightness) */
	for (i=255 ; i!=0 ; i-=5 ){
		PWM1out(i);
		delay(10);
	}

}

void lcd_display_view_notif_renderer(struct notif_ui_data *notif_data, Adafruit_LCD *lcd) {

	/* Clear and fill screen */
	graphicsMode();
	fillScreen(lcd, RA8875_DARKGREEN);
	drawNotificationBorder(lcd);
	textMode();

	/* Write app info to LCD*/
	textSetCursor(40, 43);
	textEnlarge(lcd,2);
	textColor(RA8875_WHITE, RA8875_DARKGREEN);
	textWrite(lcd, notif_data->notif->app, NOTIF_APP_SIZE);

	/* Write title info to LCD*/
	textSetCursor(40,100);
	textEnlarge(lcd,2);
	textColor(RA8875_WHITE, RA8875_DARKGREEN);
	textWrite(lcd, notif_data->notif->title, NOTIF_TITLE_SIZE);
	OSTimeDly(OS_TICKS_PER_SEC);

	/* Write notification content */
	textSetCursor(40,170);
	textEnlarge(lcd,1);
	textColor(RA8875_WHITE, RA8875_DARKGREEN);
	textWrite(lcd, notif_data->notif->text, NOTIF_TEXT_SIZE);

}


