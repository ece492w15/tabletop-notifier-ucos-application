/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LCD_DISPLAY_RENDERERS_H
#define LCD_DISPLAY_RENDERERS_H

#include <stdio.h>
#include <stdlib.h>
#include "../../lcd/Adafruit_LCD.h"
#include "../renderers.h"

/* Initialize the TFT LCD display */
Adafruit_LCD *init_tft_lcd(int lcd_width, int lcd_height);

/* Task specific rendering functions */
void lcd_display_time_renderer(struct time_ui_data *time_data, Adafruit_LCD *lcd);
void lcd_display_new_notif_renderer(struct notif_ui_data *notif_data, Adafruit_LCD *lcd);
void lcd_display_view_notif_renderer(struct notif_ui_data *notif_data, Adafruit_LCD *lcd);

#endif
