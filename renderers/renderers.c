/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "renderers.h"
#define lcd_width	800
#define lcd_height	480


void *renderer_init(void) {
	switch (DISPLAY) {
	case TFT_LCD:
		return (void *) init_tft_lcd(lcd_width, lcd_height);
	case CHAR_LCD:
		return (void *) init_lcd();
	default:
		return NULL;
	}
}

int accept_ui_lock(const char *task_name) {
	unsigned char err;
	int accepted = OSMutexAccept(ui_lock, &err);
	if (err != OS_NO_ERR) {
		printf("%s: Error accepting ui_lock\n", task_name);
		mux_accept_err(err);
	}
	return accepted;
}

unsigned char acquire_ui_lock(const char *task_name, int timeout) {
	unsigned char err;
	OSMutexPend(ui_lock, timeout * OS_TICKS_PER_SEC, &err);
	if (err != OS_NO_ERR) {
		printf("%s: Error pending on ui_lock\n", task_name);
		mux_pend_err(err);
	}
	return err;
}

unsigned char release_ui_lock(const char *task_name) {
	unsigned char err;
	err = OSMutexPost(ui_lock);
	if (err != OS_NO_ERR) {
		printf("%s: Error posting to ui_lock\n", task_name);
		mux_post_err(err);
	}
	return err;
}

void time_renderer(void *data, void *device) {
	struct time_ui_data *time_data = (struct time_ui_data *) data;

	switch (DISPLAY) {
	case TFT_LCD: {
		Adafruit_LCD *lcd = (Adafruit_LCD *) device;
		lcd_display_time_renderer(time_data, lcd);
		return;
	}
	case CHAR_LCD: {
		alt_up_character_lcd_dev *lcd = (alt_up_character_lcd_dev *) device;
		char_display_time_renderer(time_data, lcd);
		return;
	}
	default:
		log_time_renderer(time_data, NULL);
		return;
	}
}

void new_notif_renderer(void *data, void *device) {
	struct notif_ui_data *notif_data = (struct notif_ui_data *) data;

	switch (DISPLAY) {
	case TFT_LCD: {
		Adafruit_LCD *lcd = (Adafruit_LCD *) device;
		lcd_display_new_notif_renderer(notif_data, lcd);
		return;
	}
	case CHAR_LCD: {
		alt_up_character_lcd_dev *lcd = (alt_up_character_lcd_dev *) device;
		char_display_new_notif_renderer(notif_data, lcd);
		return;
	}
	default:
		log_new_notif_renderer(notif_data, NULL);
		return;
	}
}

void view_notif_renderer(void *data, void *device) {
	struct view_notif_ui_data *notif_data = (struct view_notif_ui_data *) data;

	switch (DISPLAY) {
	case TFT_LCD: {
		Adafruit_LCD *lcd = (Adafruit_LCD *) device;
		lcd_display_view_notif_renderer(notif_data, lcd);
		return;
	}
	case CHAR_LCD: {
		alt_up_character_lcd_dev *lcd = (alt_up_character_lcd_dev *) device;
		char_display_view_notif_renderer(notif_data, lcd);
		return;
	}
	default:
		log_new_notif_renderer(notif_data, NULL);
		return;
	}
}

