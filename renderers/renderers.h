/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: renderers.h
 *
 * Renderers are the functions responsible for formatting data from other
 * system tasks and displaying them to the output device. This file contains
 * function prototypes related to rendering data and managing output devices.
 */
#ifndef RENDERERS_H
#define RENDERERS_H

#include <stdio.h>
#include "../lcd/Adafruit_LCD.h"
#include "altera_up_avalon_character_lcd.h"
#include "system.h"
#include "../utils/error_handlers.h"
#include "../tasks/notification_task.h"
#include "../tasks/time_task.h"
#include "../tasks/view_notification_task.h"
#include "char_display_renderer/char_display_renderers.h"
#include "log_renderer/log_renderers.h"

#define TFT_LCD 0
#define CHAR_LCD 1
#define LOG 2

#ifdef ALT_MODULE_CLASS_spi_LCD
#define DISPLAY TFT_LCD
#else
#define DISPLAY CHAR_LCD
#endif

extern OS_EVENT *ui_lock;

/* Type containing data and its associated render function */
struct ui_frame {
	void *data; 						// Data to render to the UI
	void (* render)(void *, void *); 	// Task specific render function
	void (* reclaim)(void *);			// Task specific garbage collection function
};

/* Initialize the required devices for this renderer set */
void *renderer_init(void);

/* UI lock acquisition and release */
int accept_ui_lock(const char *task_name);
unsigned char acquire_ui_lock(const char *task_name, int timeout);
unsigned char release_ui_lock(const char *task_name);

/* Task specific rendering functions */
void time_renderer(void *data, void *device);
void new_notif_renderer(void *data, void *device);
void view_notif_renderer(void *data, void *device);

#endif
