/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "log_renderers.h"

void log_time_renderer(struct time_ui_data *time_data, void *device) {
	printf("log_renderer: current time is %s\n", time_data->time_str);
	printf("log_renderer: %s\n", time_data->notifs_str);
}

void log_new_notif_renderer(struct notif_ui_data *notif_data, void *device) {
	printf("log_renderer: new notification from %s:\n", notif_data->notif->app);
	printf("  Title: %s\n  Text: %s\n", notif_data->notif->title, notif_data->notif->text);
}
