/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef LOG_RENDERERS_H
#define LOG_RENDERERS_H

#include "../renderers.h"

/* Renderer which outputs the data to stdout */
void log_time_renderer(struct time_ui_data *time_data, void *);
void log_new_notif_renderer(struct notif_ui_data *notif_data, void *);

#endif
