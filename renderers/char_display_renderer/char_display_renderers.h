/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef CHAR_DISPLAY_RENDERERS_H
#define CHAR_DISPLAY_RENDERERS_H

#include <stdio.h>
#include <stdlib.h>
#include "altera_up_avalon_character_lcd.h"
#include "../renderers.h"

/* Initialize the 16x2 character LCD display */
alt_up_character_lcd_dev *init_lcd(void);

/* Task specific rendering functions */
void char_display_time_renderer(struct time_ui_data *time_data, alt_up_character_lcd_dev *lcd);
void char_display_new_notif_renderer(struct notif_ui_data *notif_data, alt_up_character_lcd_dev *lcd);
void char_display_next_notif_renderer(struct time_ui_data *time_data, alt_up_character_lcd_dev *lcd);

/* Utility functions */
int cursor_top(alt_up_character_lcd_dev *lcd);
int cursor_bot(alt_up_character_lcd_dev *lcd);
void cursor_write(alt_up_character_lcd_dev *lcd, const char *ptr);
void write_top(alt_up_character_lcd_dev *lcd, const char *ptr);
void write_bot(alt_up_character_lcd_dev *lcd, const char *ptr);

#endif
