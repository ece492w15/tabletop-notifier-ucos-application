/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "char_display_renderers.h"

alt_up_character_lcd_dev *init_lcd(void) {
	/* Open device */
	alt_up_character_lcd_dev *lcd;
	lcd = alt_up_character_lcd_open_dev(CHARACTER_LCD_0_NAME);
	if (lcd == NULL) {
		printf("renderer: Error opening character LCD device\n");
	} else {
		printf("renderer: Opened character LCD device\n");
	}

	/* Initialize the display */
	alt_up_character_lcd_init(lcd);
	return lcd;
}

void char_display_time_renderer(struct time_ui_data *time_data,
		alt_up_character_lcd_dev *lcd) {
	alt_up_character_lcd_init(lcd);

	/* Write the time in the top row */
	write_top(lcd, time_data->time_str);

	/* Write the notification data in the bottom row */
	write_bot(lcd, time_data->notifs_str);
}

void char_display_new_notif_renderer(struct notif_ui_data *notif_data,
		alt_up_character_lcd_dev *lcd) {
	alt_up_character_lcd_init(lcd);

	/* Write the app and title to the top row */
	write_top(lcd, notif_data->notif->app);

	/* Write the text to the bottom row */
	write_bot(lcd, notif_data->notif->title);
}

void char_display_view_notif_renderer(struct notif_ui_data *notif_data,
		alt_up_character_lcd_dev *lcd) {
	alt_up_character_lcd_init(lcd);

	/* Write the app and title to the top row */
	write_top(lcd, notif_data->notif->app);

	/* Write the text to the bottom row */
	write_bot(lcd, notif_data->notif->title);

	OSTimeDly(2*OS_TICKS_PER_SEC);

	alt_up_character_lcd_init(lcd);
	write_top(lcd, notif_data->notif->text);
}

/* Utility functions */

int cursor_top(alt_up_character_lcd_dev *lcd) {
	return alt_up_character_lcd_set_cursor_pos(lcd, 0, 0);
}

int cursor_bot(alt_up_character_lcd_dev *lcd) {
	return alt_up_character_lcd_set_cursor_pos(lcd, 0, 1);
}

void cursor_write(alt_up_character_lcd_dev *lcd, const char *ptr) {
	alt_up_character_lcd_string(lcd, ptr);
}

void write_top(alt_up_character_lcd_dev *lcd, const char *ptr) {
	if (cursor_top(lcd) == 0) {
		cursor_write(lcd, ptr);
	}
}

void write_bot(alt_up_character_lcd_dev *lcd, const char *ptr) {
	if (cursor_bot(lcd) == 0) {
		cursor_write(lcd, ptr);
	}
}
