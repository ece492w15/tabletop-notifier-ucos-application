/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "buttons.h"

void init_button_interrupts(void) {
	// TODO: Get rid of some code duplication in this method
	int e = alt_ic_isr_register(KEY_1_IRQ_INTERRUPT_CONTROLLER_ID,
			KEY_1_IRQ, button_1_input_isr, (void *) key_1_event, NULL);
	if (e != 0) {
		printf("Error registering interrupt for button 1.\n");
	}
	e = alt_ic_irq_enable(KEY_1_IRQ_INTERRUPT_CONTROLLER_ID, KEY_1_IRQ);
	if (e != 0) {
		printf("Error enabling interrupt for button 1.\n");
	}

	e = alt_ic_isr_register(KEY_2_IRQ_INTERRUPT_CONTROLLER_ID,
			KEY_2_IRQ, button_2_input_isr, (void *) key_2_event, NULL);
	if (e != 0) {
		printf("Error registering interrupt for button 2.\n");
	}
	e = alt_ic_irq_enable(KEY_2_IRQ_INTERRUPT_CONTROLLER_ID, KEY_2_IRQ);
	if (e != 0) {
		printf("Error enabling interrupt for button 2.\n");
	}

	e = alt_ic_isr_register(KEY_3_IRQ_INTERRUPT_CONTROLLER_ID,
			KEY_3_IRQ, button_3_input_isr, (void *) key_3_event, NULL);
	if (e != 0) {
		printf("Error registering interrupt for button 3.\n");
	}
	e = alt_ic_irq_enable(KEY_3_IRQ_INTERRUPT_CONTROLLER_ID, KEY_3_IRQ);
	if (e != 0) {
		printf("Error enabling interrupt for button 3.\n");
	}

	/* Unmask the button interrupts */
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEY_1_BASE, 0xf);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEY_2_BASE, 0xf);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(KEY_3_BASE, 0xf);
}

void button_1_input_isr(void *isr_context) {
	OSMboxPost((OS_EVENT *) isr_context, (void *) KEY_1_EVENT);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEY_1_BASE, 0);
}

void button_2_input_isr(void *isr_context) {
	OSMboxPost((OS_EVENT *) isr_context, (void *) KEY_2_EVENT);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEY_2_BASE, 0);
}

void button_3_input_isr(void *isr_context) {
	OSMboxPost((OS_EVENT *) isr_context, (void *) KEY_3_EVENT);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(KEY_3_BASE, 0);
}
