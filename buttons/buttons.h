/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename:buttons.h
 *
 * Interrupt service routines for push button IO.
 */

#ifndef BUTTONS_H
#define BUTTONS_H

#include <stdio.h>
#include <stdlib.h>
#include <ucos_ii.h>
#include <sys/alt_irq.h>
#include "altera_avalon_pio_regs.h"
#include "system.h"

#define KEY_3_EVENT 3
#define KEY_2_EVENT 2
#define KEY_1_EVENT 1

extern OS_EVENT *key_1_event;
extern OS_EVENT *key_2_event;
extern OS_EVENT *key_3_event;

void init_button_interrupts(void);
void button_1_input_isr(void *isr_context);
void button_2_input_isr(void *isr_context);
void button_3_input_isr(void *isr_context);

#endif
