/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "notifications.h"

int json_notif_read(const char *buf, struct notification *notif) {
	/* Mapping of JSON attributes to C object's struct members */
	const struct json_attr_t json_attrs[] = {
			{"app", t_string, .addr.string = notif->app, .len = sizeof(notif->app)},
			{"title", t_string, .addr.string = notif->title, .len = sizeof(notif->title)},
			{"text", t_string, .addr.string = notif->text, .len = sizeof(notif->text)},
			{NULL},
	};
	/* Parse the JSON object from buffer */
	return json_read_object(buf, json_attrs, NULL);
}
