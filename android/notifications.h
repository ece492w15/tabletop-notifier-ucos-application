/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: notifications.h
 *
 * Android notifications are represented and parsed using the structures and
 * functions included here. Notifications are received in JSON format and
 * parsed using the microjson library.
 */

#ifndef NOTIFICATIONS_H
#define NOTIFICATIONS_H

#include "../mjson/mjson.h"

#define NOTIF_APP_SIZE 26		// TODO: These sizes are arbitrarily chosen and
#define NOTIF_TITLE_SIZE 26	// will require review.
#define NOTIF_TEXT_SIZE 256		//

/* Notification model */
struct notification {
	char app[NOTIF_APP_SIZE];
	char title[NOTIF_TITLE_SIZE];
	char text[NOTIF_TEXT_SIZE];
};

/* Notification parsing function reads the JSON string contained within the
 * char buffer and stores the parsed fields into the notification structure */
int json_notif_read(const char *buf, struct notification *notif);

#endif
