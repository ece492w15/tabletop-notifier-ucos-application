/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: main.c
 *
 * The main program defines program parameters such as stack sizes and queue
 * sizes in addition to initializing references to shared operating system
 * data structures (semaphores, queues, ...). Creates all tasks required for
 * operation and begins multitasking.
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ucos_ii.h>
#include <sys/alt_irq.h>
#include "includes.h"
#include "android/notifications.h"
#include "buttons/buttons.h"
#include "renderers/renderers.h"
#include "system.h"
#include "tasks/bt_comm_task.h"
#include "tasks/command_task.h"
#include "tasks/input_consumer_task.h"
#include "tasks/notification_task.h"
#include "tasks/set_time_task.h"
#include "tasks/time_task.h"
#include "tasks/ui_task.h"
#include "tasks/view_notification_task.h"
#include "utils/error_handlers.h"

#define lcd_width	800
#define lcd_height	480


/* Task stack sizes (TODO: optimize for individual tasks) */
#define TASK_STACKSIZE_DEFAULT 2048

/* Supported data quantities */
#define MAX_MESSAGES 64		// maximum number of unprocessed android messages
#define MAX_NOTIFS 64		// maximum number of unread notifications
#define MAX_UI_FRAMES 8	// maximum number of UI frames in queue
#define MAX_COMMANDS 8		// maximum number of android commands in queue

/* Task priorities */
#define TEST_INPUT_PRIO				4
#define UI_PIP 						5	// UI lock priority inheritance priority (PIP)
#define UI_TASK_PRIO 				6	// Should be high priority for responsiveness
#define NOTIFICATION_TASK_PRIO 		7
#define COMMAND_TASK_PRIO			8
#define VIEW_NOTIF_TASK_PRIO		9
#define BT_COMM_TASK_PRIO 			10
#define TIME_TASK_PRIO 				11
#define SET_TIME_TASK_PRIO			12

/* Low priorities for pushbutton input consumers */
#define KEY_1_CONSUMER_TASK_PRIO OS_LOWEST_PRIO-4
#define KEY_2_CONSUMER_TASK_PRIO OS_LOWEST_PRIO-5
#define KEY_3_CONSUMER_TASK_PRIO OS_LOWEST_PRIO-6

/* Allocate OS tasks */
OS_STK bt_comm_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK notification_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK ui_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK time_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK set_time_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK command_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK view_notif_task_stk[TASK_STACKSIZE_DEFAULT];

OS_STK key_1_consumer_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK key_2_consumer_task_stk[TASK_STACKSIZE_DEFAULT];
OS_STK key_3_consumer_task_stk[TASK_STACKSIZE_DEFAULT];

/* Message queues */
OS_EVENT *android_queue;				// Message queue containing data from
void *android_msg[MAX_MESSAGES];		// Android device
OS_EVENT *notification_queue; 			// Message queue containing unread
void *notification_msg[MAX_NOTIFS]; 	// notifications
OS_EVENT *ui_frame_queue;				// Message queue containing UI updates
void *ui_frame_msg[MAX_UI_FRAMES];		// for rendering
OS_EVENT *android_cmd_queue; 			// Message queue containing commands
void *android_cmd[MAX_COMMANDS];		// from Android device

/* Mailboxes */
OS_EVENT *time_updates;
OS_EVENT *key_3_event;
OS_EVENT *key_2_event;
OS_EVENT *key_1_event;

/* Mutex Locks */
OS_EVENT *ui_lock; // Lock on the UI task

/* Test Stuff */
OS_STK test_new_notification_task_stk[TASK_STACKSIZE_DEFAULT];
#include "test/test_new_notification_task.h"

int main(void) {
	unsigned char err;

	/* Initialize message queues */
	ui_frame_queue = OSQCreate(&ui_frame_msg[0], MAX_UI_FRAMES);
	notification_queue = OSQCreate(&notification_msg[0], MAX_NOTIFS);
	android_queue = OSQCreate(&android_msg[0], MAX_MESSAGES);
	android_cmd_queue = OSQCreate(&android_cmd[0], MAX_COMMANDS);

	/* Initialize mailboxes */
	time_updates = OSMboxCreate(NULL);
	key_1_event = OSMboxCreate(NULL);
	key_2_event = OSMboxCreate(NULL);
	key_3_event = OSMboxCreate(NULL);

	/* Initialize UI lock */
	ui_lock = OSMutexCreate(UI_PIP, &err);

	/* Check for errors initializing UI lock */
	while (err != OS_NO_ERR) {
		printf("main: Error creating UI lock\n");
		mux_create_err(err);
		printf("main: retrying...\n");
		OSTimeDly(OS_TICKS_PER_SEC);
		ui_lock = OSMutexCreate(UI_PIP, &err);
	}

	/* LCD Reset*/
	* (long*)LCD_RST_BASE=0x00;
	printf("One\n");
	delay(1000);
	* (long*)LCD_RST_BASE=0x01;
	delay(1000);

	/* Bluetooth Reset */
	bool *BT_CMD = (bool*)BT_CMD_BASE;

	FILE* fp;
	fp = fopen ("/dev/uart_0", "r+");

	*BT_CMD = 0x0;

	char* msg = "D\r\n"; //show stats
	fwrite (msg, strlen (msg), 1, fp);
	delay(1000);

	msg = "R,1\r\n";  //reboot
	fwrite (msg, strlen (msg), 1, fp);
	delay(1000);

	*BT_CMD = 0x1;
	delay(1000);

	/* Initialize low priority key input consumer tasks */
	OSTaskCreateExt(input_consumer_task,
			(void *) key_1_event,
			(void *) &key_1_consumer_task_stk[TASK_STACKSIZE_DEFAULT-1],
			KEY_1_CONSUMER_TASK_PRIO,
			KEY_1_CONSUMER_TASK_PRIO,
			key_1_consumer_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(input_consumer_task,
			(void *) key_2_event,
			(void *) &key_2_consumer_task_stk[TASK_STACKSIZE_DEFAULT-1],
			KEY_2_CONSUMER_TASK_PRIO,
			KEY_2_CONSUMER_TASK_PRIO,
			key_2_consumer_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(input_consumer_task,
			(void *) key_3_event,
			(void *) &key_3_consumer_task_stk[TASK_STACKSIZE_DEFAULT-1],
			KEY_3_CONSUMER_TASK_PRIO,
			KEY_3_CONSUMER_TASK_PRIO,
			key_3_consumer_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);

	/* Initialize application tasks */
	OSTaskCreateExt(notification_task,
			NULL,
			(void *) &notification_task_stk[TASK_STACKSIZE_DEFAULT-1],
			NOTIFICATION_TASK_PRIO,
			NOTIFICATION_TASK_PRIO,
			notification_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(ui_task,
			NULL,
			(void *) &ui_task_stk[TASK_STACKSIZE_DEFAULT-1],
			UI_TASK_PRIO,
			UI_TASK_PRIO,
			ui_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(time_task,
			NULL,
			(void *) &time_task_stk[TASK_STACKSIZE_DEFAULT-1],
			TIME_TASK_PRIO,
			TIME_TASK_PRIO,
			time_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(command_task,
			NULL,
			(void *) &command_task_stk[TASK_STACKSIZE_DEFAULT-1],
			COMMAND_TASK_PRIO,
			COMMAND_TASK_PRIO,
			command_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(set_time_task,
			NULL,
			(void *) &set_time_task_stk[TASK_STACKSIZE_DEFAULT-1],
			SET_TIME_TASK_PRIO,
			SET_TIME_TASK_PRIO,
			set_time_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(view_notification_task,
			NULL,
			(void *) &view_notif_task_stk[TASK_STACKSIZE_DEFAULT-1],
			VIEW_NOTIF_TASK_PRIO,
			VIEW_NOTIF_TASK_PRIO,
			view_notif_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);
	OSTaskCreateExt(bt_comm_task,
			NULL,
			(void *) &bt_comm_task_stk[TASK_STACKSIZE_DEFAULT-1],
			BT_COMM_TASK_PRIO,
			BT_COMM_TASK_PRIO,
			bt_comm_task_stk,
			TASK_STACKSIZE_DEFAULT,
			NULL,
			OS_TASK_OPT_NONE);

	/* Enable button interrupts */
	init_button_interrupts();

	/* Begin multitasking */
	OSStart();
	return 0;
}

