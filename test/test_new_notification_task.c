#include "test_new_notification_task.h"

void test_new_notification_task(void *pdata) {
	unsigned char err;
	int counter = 0;

	while (1) {

		OSMboxPend(key_3_event, 0, &err);
		char *buf = (char *) malloc(sizeof(char)*4096);
		sprintf(buf, "{\"app\": \"app name %d\", \"title\": \"notif title %d\", \"text\": \"notif text %d\"}", counter, counter, counter);
		err = OSQPost(android_queue, (void *) buf);
		if (err != OS_NO_ERR) {
			printf("TEST: Error posting to android_queue.\n");
			q_post_err(err);
		}
		counter++;

		OSMboxPend(key_2_event, 0, &err);
		buf = (char *) malloc(sizeof(char)*4096);
		sprintf(buf, "{\"app\": \"TabletopNotifierCommand\", \"title\": \"set_time\", \"text\": \"1\"}");
		err = OSQPost(android_queue, (void *) buf);
		if (err != OS_NO_ERR) {
			printf("TEST: Error posting to android_queue.\n");
			q_post_err(err);
		}
	}
}

