
#ifndef TEST_NEW_NOTIFICATION_TASK_H
#define TEST_NEW_NOTIFICATION_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <ucos_ii.h>
#include "../utils/error_handlers.h"
#include "system.h"

extern OS_EVENT *key_3_event;
extern OS_EVENT *key_2_event;
extern OS_EVENT *key_1_event;
extern OS_EVENT *android_queue;

void test_new_notification_task(void *pdata);

#endif
