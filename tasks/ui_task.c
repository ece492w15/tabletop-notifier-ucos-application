/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ui_task.h"

const char* UI_TASK_NAME = "ui_task";

void ui_task(void *pdata) {
	printf("%s: initializing...\n", UI_TASK_NAME);
	unsigned char err;

	void *output_device = renderer_init();

	while (1) {
		/* Wait for ui_frame(s) to become available by pending on the frame queue */
		struct ui_frame *frame = (struct ui_frame *) OSQPend(ui_frame_queue, 0, &err);

		if (err != OS_NO_ERR) {
			printf("%s: Error pending on ui_frame_queue\n", UI_TASK_NAME);
			q_pend_err(err);

			/* Skip the rest of the loop if pending fails */
			continue;
		}

		/* Render the UI frame */
		(frame->render)(frame->data, output_device);

		/* Lastly, free the ui_frame and its data so we don't leak memory */
		(frame->reclaim)(frame->data);
		free(frame);
	}
}
