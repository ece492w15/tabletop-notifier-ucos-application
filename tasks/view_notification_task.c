/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "view_notification_task.h"

const char *VIEW_NOTIF_TASK_NAME = "view_notification_task";

void view_notification_task(void *pdata) {
	unsigned char err;

	while (1) {
		OSMboxPend(key_1_event, 0, &err);
		if (err != OS_NO_ERR) {
			printf("%s: Error pending on key_1_event.\n", VIEW_NOTIF_TASK_NAME);
			mbox_pend_err(err);
			continue;
		}

		acquire_ui_lock(VIEW_NOTIF_TASK_NAME, VIEW_NOTIF_DISPLAY_TIMEOUT);
		display_notifications();
		release_ui_lock(VIEW_NOTIF_TASK_NAME);
	}
}

void display_notifications() {
	unsigned char err;

	/* While there are more unread notifications, show each of them in succession */
	while (more_notifications()) {
		struct notification *notif =
				(struct notification *) OSQPend(notification_queue, 0, &err);
		if (err != OS_NO_ERR) {
			printf("%s: Error pending on notification_queue.\n", VIEW_NOTIF_TASK_NAME);
			q_pend_err(err);
			continue;
		}

		display_notification_content(notif);
		OSMboxPend(key_1_event, 0, &err);
	}
}

void display_notification_content(struct notification *notif) {
	unsigned char err;

	/* Allocate data for view_notif_ui_data - free in UI task */
	struct view_notif_ui_data *notif_data =
			(struct view_notif_ui_data *) malloc(sizeof(struct view_notif_ui_data));
	if (notif_data == NULL) {
		printf("%s: Error allocating view_notif_ui_data.\n", VIEW_NOTIF_TASK_NAME);
		return;
	}

	/* Populate the notification data for the UI */
	notif_data->notif = notif;

	/* Allocate UI frame */
	struct ui_frame *frame = (struct ui_frame *) malloc(sizeof(struct ui_frame));
	if (frame == NULL) {
		printf("%s: Error allocating UI frame.\n", VIEW_NOTIF_TASK_NAME);
		free(notif_data);
		return;
	}

	/* Populate frame with the notification data and renderer */
	frame->data = (void *) notif_data;
	frame->render = &view_notif_renderer;
	frame->reclaim = &reclaim_view_notif_ui_data;

	/* If an error occurs, free the notification ui data and frame. Do not
	 * free the notification struct yet, as we will still want to try to
	 * post it to the unread notifications queue. */
	if (err != OS_NO_ERR) {
		free(notif_data);
		free(frame);
		return;
	}

	err = OSQPost(ui_frame_queue, (void *) frame);
	if (err != OS_NO_ERR) {
		/* An error occurred posting the UI frame to the UI task, release
		 * the lock and free the ui data and frame. Do not free the
		 * notification struct yet. */
		free(notif_data);
		free(frame);
		return;
	}
}

bool more_notifications() {
	unsigned char err;
	OS_Q_DATA qdata;

	err = OSQQuery(notification_queue, &qdata);
	if (err != OS_NO_ERR)
	{
		printf("%s: error occurred while retrieving queue info\n", VIEW_NOTIF_TASK_NAME);
		q_query_err(err);
		return false;
	}
	if (qdata.OSNMsgs > 0) {
		return true;
	}
	return false;
}

void reclaim_view_notif_ui_data(void *data) {
	struct notif_ui_data *notif_data = (struct notif_ui_data *) data;
	free(notif_data->notif);
}
