/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: ui_task.h
 *
 * The UI task is responsible for rendering data from other system tasks to
 * an available output. The task takes ui_frame structs from a queue and
 * executes the referenced render function on the data from the struct.
 */

#ifndef UI_TASK_H
#define UI_TASK_H

#include <stdlib.h>
#include <ucos_ii.h>
#include "../renderers/renderers.h"
#include "../utils/error_handlers.h"

extern OS_EVENT *ui_frame_queue;

void ui_task(void *pdata);

#endif
