/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "input_consumer_task.h"

const char *INPUT_CONSUMER_TASK_NAME = "input_consumer_task";

void input_consumer_task(void *pdata) {
	OS_EVENT *input_mbox = (OS_EVENT *) pdata;
	unsigned char err;

	while (1) {
		OSMboxPend(input_mbox, 0, &err);
		if (err != OS_NO_ERR) {
			printf("%s: An error occurred while pending on mailbox.\n",
					INPUT_CONSUMER_TASK_NAME);
			mbox_pend_err(err);
		}
	}
}
