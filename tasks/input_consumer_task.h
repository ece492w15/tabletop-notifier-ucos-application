/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: input_consumer_task.h
 *
 * The Input Consumer Task is intended to run at a low priority. The task is
 * responsible for sinking unused button inputs posted to the key button
 * mailboxes.
 */

#ifndef INPUT_CONSUMER_TASK_H
#define INPUT_CONSUMER_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <ucos_ii.h>
#include "../utils/error_handlers.h"

void input_consumer_task(void *pdata);

#endif
