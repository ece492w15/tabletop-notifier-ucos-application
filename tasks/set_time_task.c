/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "set_time_task.h"

const char *SET_TIME_TASK_NAME = "set_time_task";

void set_time_task(void *pdata) {
	unsigned char err;

	while (1) {
		/* Check if the time is being set by a command */
		void *updated_time = OSMboxPend(time_updates, 0, &err);
		if (err != OS_NO_ERR) {
			printf("%s: Error pending on time_updates.\n", SET_TIME_TASK_NAME);
			mbox_pend_err(err);
			continue;
		}
		if (updated_time != NULL) {
			OSTimeSet((int) updated_time*OS_TICKS_PER_SEC);
		}
	}
}
