/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: view_notification_task.h
 *
 * This task is responsible for displaying unread notifications to the user
 * when prompted by input.
 */

#ifndef VIEW_NOTIFICATION_TASK_H
#define VIEW_NOTIFICATION_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <ucos_ii.h>
#include "../utils/error_handlers.h"
#include "../android/notifications.h"
#include "../renderers/renderers.h"

#define VIEW_NOTIF_DISPLAY_TIMEOUT 0

struct view_notif_ui_data {
	struct notification *notif;
};

extern OS_EVENT *notification_queue;
extern OS_EVENT *ui_frame_queue;
extern OS_EVENT *key_1_event;

void view_notification_task(void *pdata);
void display_notifications();
void reclaim_view_notif_ui_data(void *data);
bool more_notifications();

#endif
