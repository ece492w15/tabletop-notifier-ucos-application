/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: notification_task.h
 *
 * The notification_task is responsible for receiving notifications from the
 * paired Android device via the bluetooth module and posting them to the
 * queue.
 */

#ifndef NOTIFICATION_TASK_H
#define NOTIFICATION_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucos_ii.h>
#include "../android/notifications.h"
#include "../utils/error_handlers.h"
#include "../renderers/renderers.h"

#define NOTIF_DISPLAY_TIMEOUT 1
#define NOTIF_DISPLAY_DURATION 3

extern OS_EVENT *android_queue;
extern OS_EVENT *android_cmd_queue;
extern OS_EVENT *notification_queue;
extern OS_EVENT *ui_lock;

struct notif_ui_data {
	struct notification *notif;
};

void notification_task(void *pdata);
bool is_command(struct notification *notif);
void post_command(struct notification *notif);
void display_notification(struct notification *notif);
void post_notification(struct notification *notif);
void reclaim_notif_ui_data(void *data);

#endif
