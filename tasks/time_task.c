/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "time_task.h"
#include <string.h>

const char *TIME_TASK_NAME = "time_task";

void time_task(void *pdata) {
	printf("time_task: initializing...\n");
	unsigned char err;
	int minute = -1;
	int notif_num = -1;


	while (1) {

		/* Allocate time and notification count buffer - freed in ui_task */
		char *time_buffer = (char *) malloc(TIME_BUFF_SIZE*sizeof(char));
		if (time_buffer == NULL) {
			printf("time_task: malloc for time buffer failed\n");
			continue;
		}

		char *notif_count_buffer = (char *) malloc(NOTIF_CNT_BUFF_SIZE*sizeof(char));
		if (notif_count_buffer == NULL) {
			printf("time_task: malloc for notification count buffer failed\n");
			free(time_buffer);
			continue;
		}


		/* Allocate time_ui_data - freed in ui_task */
		struct time_ui_data *ui_data = (struct time_ui_data *) malloc(sizeof(struct time_ui_data));
		if (ui_data == NULL) {
			/* Can't allocate memory for ui_data, free the time_buffer and retry */
			printf("ui_task: unable to allocate memory for time_ui_data\n");
			free(notif_count_buffer);
			free(time_buffer);
			continue;
		}

		ui_data->time_str = time_buffer;
		ui_data->notifs_str = notif_count_buffer;

		/* Allocate ui_frame - freed in ui_task */
		struct ui_frame *frame = (struct ui_frame *) malloc(sizeof(struct ui_frame));
		if (frame == NULL) {
			/* Can't allocate memory for ui_frame, free time_buffer and ui_data and retry */
			printf("ui_task: unable to allocate memory for ui_frame\n");
			free(notif_count_buffer);
			free(time_buffer);
			free(ui_data);
			continue;
		}

		/* Construct UI frame for ui_task */
		frame->data = ui_data;
		frame->render = &time_renderer;
		frame->reclaim = &reclaim_time_ui_data;

		/* Queue up the frame for the UI task to render. If the UI lock is not available,
		 * discard the current frame.
		 */
		int accepted = accept_ui_lock(TIME_TASK_NAME);
		if (accepted == 0) {
			free(notif_count_buffer);
			free(time_buffer);
			free(ui_data);
			free(frame);
			continue;
		}

		/* Get the local time from the system. */
		/* Check for changes in time or notification count and hold until */
		int temp2 = get_notif_count(notif_count_buffer, NOTIF_CNT_BUFF_SIZE);
		int temp = get_time(time_buffer, TIME_BUFF_SIZE);
		if ((minute != temp) | (notif_num != temp2)){

			minute = temp;
			notif_num = temp2;
			err = OSQPost(ui_frame_queue, (void *) frame);
			if (err != OS_NO_ERR) {
				release_ui_lock(TIME_TASK_NAME);
				free(notif_count_buffer);
				free(time_buffer);
				free(ui_data);
				free(frame);
				continue;
			}
		}

		release_ui_lock(TIME_TASK_NAME);

		/* Update every second */
		OSTimeDly(OS_TICKS_PER_SEC);
	}
}

int get_time(char *buf, int n) {
	int seconds = (OSTimeGet()/OS_TICKS_PER_SEC) % SECONDS_PER_DAY;
	int hr = seconds / SECONDS_PER_HOUR;
	int r = seconds % SECONDS_PER_HOUR;
	int min = r / SECONDS_PER_MINUTE;
	int sec = r % SECONDS_PER_MINUTE;
	snprintf(buf, n, "%02d:%02d", hr, min);
	return min;
}

int get_notif_count(char *buf, int n) {
	unsigned char err;
	OS_Q_DATA qdata;

	err = OSQQuery(notification_queue, &qdata);
	if (err != OS_NO_ERR)
	{
		printf("ui_task: error occurred while retrieving queue info\n");
		q_query_err(err);
		snprintf(buf, n, "- notifications");
		//		return;
	}
	if (qdata.OSNMsgs == 1){
		snprintf(buf, n, "%d notification ", qdata.OSNMsgs);
	} else{
		snprintf(buf, n, "%d notifications", qdata.OSNMsgs);
	}

	return qdata.OSNMsgs;
}

void reclaim_time_ui_data(void *data) {
	struct time_ui_data *time_data = (struct time_ui_data *) data;
	free(time_data->time_str);
	free(time_data->notifs_str);
}
