/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: time_task.h
 *
 * The time_task is responsible for keeping the displayed time and notification
 * count up to date.
 */
#ifndef TIME_TASK_H
#define TIME_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <ucos_ii.h>
#include "../renderers/renderers.h"
#include "../utils/error_handlers.h"

#define SECONDS_PER_DAY 86400
#define SECONDS_PER_HOUR 3600
#define SECONDS_PER_MINUTE 60

#define TIME_BUFF_SIZE 6
#define NOTIF_CNT_BUFF_SIZE 16
#define TIME_DISPLAY_TIMEOUT 0.5

extern OS_EVENT *time_updates;
extern OS_EVENT *notification_queue;
extern OS_EVENT *ui_frame_queue;
extern OS_EVENT *ui_lock;

struct time_ui_data {
	char *time_str;
	char *notifs_str;
};

void time_task(void *pdata);
int get_time(char *buf, int n);
int get_notif_count(char *buf, int n);
void reclaim_time_ui_data(void *data);
int update_time(int time);

#endif
