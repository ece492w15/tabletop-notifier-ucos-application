/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "notification_task.h"

const char *NOTIF_TASK_NAME = "notification_task";

void notification_task(void *pdata) {
	printf("%s: initializing...\n", NOTIF_TASK_NAME);
	unsigned char err;
	char *garbage = OSQPend(android_queue, 0, &err);

	while (1) {
		/* Wait for notification data to become available */
		char *msg_buf = OSQPend(android_queue, 0, &err);

		if (err != OS_NO_ERR) {
			printf("%s: Error pending on android_queue\n", NOTIF_TASK_NAME);
			q_pend_err(err);
			continue;
		}

		/* Parse the notification contents into a notification struct. */
		struct notification *notif =
				(struct notification *) malloc(sizeof(struct notification));
		int json_status = json_notif_read(msg_buf, notif);
		if (json_status == 0) {
			/* Check if the notification is a configuration command */
			if (is_command(notif)) {
				post_command(notif);
				free(msg_buf);
				continue;
			}
			/* Not a command, so display it and post it to the queue */
			display_notification(notif);
			post_notification(notif);
		} else {
			printf("%s: Error parsing notification\n", NOTIF_TASK_NAME);
			printf(json_error_string(json_status));
			free(notif);
		}

		/* Lastly, free the memory allocated to msg_buf */
		free(msg_buf);
	}
}

bool is_command(struct notification *notif) {
	if (strcmp(notif->app, "TabletopNotifierCommand") == 0) {
		return true;
	}
	return false;
}

void post_command(struct notification *notif) {
	unsigned char err = OSQPost(android_cmd_queue, (void *) notif);
	if (err != OS_NO_ERR) {
		printf("notification_task: Error posting to android_cmd_queue\n");
		q_post_err(err);
		free(notif);
	}
}

void display_notification(struct notification *notif) {
	unsigned char err;

	/* Allocate data for notif_ui_data - free in UI task */
	struct notif_ui_data *notif_data =
			(struct notif_ui_data *) malloc(sizeof(struct notif_ui_data));
	if (notif_data == NULL) {
		printf("%s: Error allocating notif_ui_data.\n", NOTIF_TASK_NAME);
		return;
	}

	/* Populate the notification data for the UI */
	notif_data->notif = notif;

	/* Allocate UI frame */
	struct ui_frame *frame = (struct ui_frame *) malloc(sizeof(struct ui_frame));
	if (frame == NULL) {
		printf("%s: Error allocating UI frame.\n", NOTIF_TASK_NAME);
		free(notif_data);
		return;
	}

	/* Populate frame with the notification data and renderer */
	frame->data = (void *) notif_data;
	frame->render = &new_notif_renderer;
	frame->reclaim = &reclaim_notif_ui_data;

	/* Acquire UI lock and post the frame */
	err = acquire_ui_lock(NOTIF_TASK_NAME, NOTIF_DISPLAY_TIMEOUT);

	/* If an error occurs, free the notification ui data and frame. Do not
	 * free the notification struct yet, as we will still want to try to
	 * post it to the unread notifications queue. */
	if (err != OS_NO_ERR) {
		free(notif_data);
		free(frame);
		return;
	}

	/* Task has UI lock, so post the frame to the UI frame queue */
	err = OSQPost(ui_frame_queue, (void *) frame);
	if (err != OS_NO_ERR) {
		/* An error occurred posting the UI frame to the UI task, release
		 * the lock and free the ui data and frame. Do not free the
		 * notification struct yet. */
		release_ui_lock(NOTIF_TASK_NAME);
		free(notif_data);
		free(frame);
		return;
	}

	/* Display for fixed duration before releasing lock */
	OSTimeDly(NOTIF_DISPLAY_DURATION * OS_TICKS_PER_SEC);
	release_ui_lock(NOTIF_TASK_NAME);
}

void post_notification(struct notification *notif) {
	/* If we fail here, free the notification struct so we don't leak memory */
	unsigned char err = OSQPost(notification_queue, (void *) notif);
	if (err != OS_NO_ERR) {
		printf("notification_task: Error posting to notification_queue.\n");
		q_post_err(err);
		free(notif);
	}
}

void reclaim_notif_ui_data(void *data) {
	// This should not be freed by the UI task, as the notification may be used by other
	// tasks still! Notifications should be freed after being read by the user.
#ifdef IGNORE
	struct notif_ui_data *notif_data = (struct notif_ui_data *) data;
	free(notif_data->notif);
#endif
}

