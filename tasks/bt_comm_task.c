/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include "bt_comm_task.h"
 
void bt_comm_task(void *pdata) {
	unsigned char err;

	FILE* fp = fopen ("/dev/uart_0", "r+");

	while (1) {
		/* Allocate space for incoming messages from Android device - free
		 * in notification task */
		char *msg_buf = (char *) malloc(MAX_NOTIF_SIZE*sizeof(char));

		fgets(msg_buf, MAX_NOTIF_SIZE, fp);

		/* Post the buffer to the android_queue for the notification task */
		printf(msg_buf);
		err = OSQPost(android_queue, (void *) msg_buf);

		if (err != OS_NO_ERR) {
			printf("bt_comm_task: Error posting to android_queue\n");
			q_post_err(err);
			free(msg_buf);
		}
	}
}
