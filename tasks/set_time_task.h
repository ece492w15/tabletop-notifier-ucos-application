/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: set_time_task.h
 *
 * The set_time_task is responsible for accepting commands from the Android
 * device to set the current system time.
 */

#ifndef SET_TIME_TASK_H
#define SET_TIME_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <ucos_ii.h>
#include "../utils/error_handlers.h"

extern OS_EVENT *time_updates;

void set_time_task(void *pdata);

#endif
