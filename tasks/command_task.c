/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "command_task.h"

const char *COMMAND_TASK_NAME = "command_task";

void command_task(void *pdata) {
	unsigned char err;

	while (1) {
		struct notification *notif = OSQPend(android_cmd_queue, 0, &err);
		if (err != OS_NO_ERR) {
			printf("%s: Error pending to android_cmd_queue.\n", COMMAND_TASK_NAME);
			q_post_err(err);
			continue;
		}

		/* Handle the command and free the resources afterward */
		send_command(notif);
		free(notif);
	}
}

unsigned char send_command(struct notification *notif) {
	unsigned char err;

	/* Check if the command is to set the time */
	if (strcmp(notif->title, ANDROID_CMD_SET_TIME) == 0) {
		int new_time = atoi(notif->text);
		printf("%s: Setting system time to %d second(s).\n", COMMAND_TASK_NAME, new_time);
		err = OSMboxPost(time_updates, (void *) new_time);
		if (err != OS_NO_ERR) {
			printf("%s: Error posting to time_updates.\n", COMMAND_TASK_NAME);
			mbox_post_err(err);
		}
	}

	/* Add additional command checks here */

	/* Return the error code */
	return err;
}
