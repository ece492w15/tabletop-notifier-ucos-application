/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: command_task.h
 *
 * The command_task is responsible for handling messages from the paired
 * Android device that are identified as configuration commands.
 */
#ifndef COMMAND_TASK_H
#define COMMAND_TASK_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ucos_ii.h>
#include "../android/notifications.h"
#include "../utils/error_handlers.h"

#define ANDROID_CMD_SET_TIME "set_time"

extern OS_EVENT *android_cmd_queue;
extern OS_EVENT *time_updates;

void command_task(void *pdata);

// Parse the title attribute of a notification to determine the intended
// command. Dispatch the command to the appropriate task.
unsigned char send_command(struct notification *notif);

#endif
