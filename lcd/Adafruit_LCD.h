/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * This file has been copied from the library for the Adafruit RA8875 TFT
 * display driver board and adapted to work with the Altera DE2.
 * This file is written to power and write to a 800 x 480 pixel
 * resolution TFT display.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef _ADAFRUIT_LCD_H
#define _ADAFRUIT_LCD_H

#include "system.h"
#include <stdlib.h>
#include <stdbool.h>
#include "alt_types.h"
#include <stdint.h>
#include <string.h>

#define swap(a, b) { alt_16 t = a; a = b; b = t; }

enum RA8875sizes { RA8875_480x272, RA8875_800x480 };

// Colors (RGB565)
#define	RA8875_BLACK           	0x0000
#define RA8875_NAVY           	0x000F
#define	RA8875_BLUE             0x5A6A
#define	RA8875_DARKBLUE         0xC352
#define	RA8875_RED              0xF805
#define	RA8875_GREEN            0x07F0
#define	RA8875_DARKGREEN        0x020
#define RA8875_DARKCYAN        	0x9FD3
#define RA8875_CYAN             0x07FF
#define RA8875_LIGHTCYAN        0x03EF
#define	RA8875_TEAL            	0x00E2
#define RA8875_MAGENTA          0xF81F
#define RA8875_DARKPURPLE		0xA30E
#define RA8875_PURPLE			0x6B71
#define RA8875_INDIGO1			0x5A6A
#define RA8875_INDIGO2			0x6A8B
#define RA8875_ORCHID			0x7392
#define RA8875_PINK         	0xF986
#define RA8875_YELLOW           0xFFF5
#define RA8875_ORANGE         	0xFC65
#define RA8875_LIGHTORANGE      0x7AAC
#define RA8875_WHITE            0xFFFF
#define	RA8875_LIGHTGREY        0xC618
#define	RA8875_DARKGREY       	0x7BEF

// Command/Data pins for SPI
#define RA8875_DATAWRITE        0x00
#define RA8875_DATAREAD         0x40
#define RA8875_CMDWRITE         0x80
#define RA8875_CMDREAD          0xC0

// Registers & bits
#define RA8875_PWRR             0x01
#define RA8875_PWRR_DISPON      0x80
#define RA8875_PWRR_DISPOFF     0x00
#define RA8875_PWRR_SLEEP       0x02
#define RA8875_PWRR_NORMAL      0x00
#define RA8875_PWRR_SOFTRESET   0x01

#define RA8875_MRWC             0x02

#define RA8875_GPIOX            0xC7

#define RA8875_PLLC1            0x88
#define RA8875_PLLC1_PLLDIV2    0x80
#define RA8875_PLLC1_PLLDIV1    0x00

#define RA8875_PLLC2            0x89
#define RA8875_PLLC2_DIV1       0x00
#define RA8875_PLLC2_DIV2       0x01
#define RA8875_PLLC2_DIV4       0x02
#define RA8875_PLLC2_DIV8       0x03
#define RA8875_PLLC2_DIV16      0x04
#define RA8875_PLLC2_DIV32      0x05
#define RA8875_PLLC2_DIV64      0x06
#define RA8875_PLLC2_DIV128     0x07

#define RA8875_SYSR             0x10
#define RA8875_SYSR_8BPP        0x00
#define RA8875_SYSR_16BPP       0x0C
#define RA8875_SYSR_MCU8        0x00
#define RA8875_SYSR_MCU16       0x03

#define RA8875_PCSR             0x04
#define RA8875_PCSR_PDATR       0x00
#define RA8875_PCSR_PDATL       0x80
#define RA8875_PCSR_CLK         0x00
#define RA8875_PCSR_2CLK        0x01
#define RA8875_PCSR_4CLK        0x02
#define RA8875_PCSR_8CLK        0x03

#define RA8875_HDWR             0x14

#define RA8875_HNDFTR           0x15
#define RA8875_HNDFTR_DE_HIGH   0x00
#define RA8875_HNDFTR_DE_LOW    0x80

#define RA8875_HNDR             0x16
#define RA8875_HSTR             0x17
#define RA8875_HPWR             0x18
#define RA8875_HPWR_LOW         0x00
#define RA8875_HPWR_HIGH        0x80

#define RA8875_VDHR0            0x19
#define RA8875_VDHR1            0x1A
#define RA8875_VNDR0            0x1B
#define RA8875_VNDR1            0x1C
#define RA8875_VSTR0            0x1D
#define RA8875_VSTR1            0x1E
#define RA8875_VPWR             0x1F
#define RA8875_VPWR_LOW         0x00
#define RA8875_VPWR_HIGH        0x80

#define RA8875_HSAW0            0x30
#define RA8875_HSAW1            0x31
#define RA8875_VSAW0            0x32
#define RA8875_VSAW1            0x33

#define RA8875_HEAW0            0x34
#define RA8875_HEAW1            0x35
#define RA8875_VEAW0            0x36
#define RA8875_VEAW1            0x37

#define RA8875_MCLR             0x8E
#define RA8875_MCLR_START       0x80
#define RA8875_MCLR_STOP        0x00
#define RA8875_MCLR_READSTATUS  0x80
#define RA8875_MCLR_FULL        0x00
#define RA8875_MCLR_ACTIVE      0x40

#define RA8875_DCR                    0x90
#define RA8875_DCR_LINESQUTRI_START   0x80
#define RA8875_DCR_LINESQUTRI_STOP    0x00
#define RA8875_DCR_LINESQUTRI_STATUS  0x80
#define RA8875_DCR_CIRCLE_START       0x40
#define RA8875_DCR_CIRCLE_STATUS      0x40
#define RA8875_DCR_CIRCLE_STOP        0x00
#define RA8875_DCR_FILL               0x20
#define RA8875_DCR_NOFILL             0x00
#define RA8875_DCR_DRAWLINE           0x00
#define RA8875_DCR_DRAWTRIANGLE       0x01
#define RA8875_DCR_DRAWSQUARE         0x10


#define RA8875_ELLIPSE                0xA0
#define RA8875_ELLIPSE_STATUS         0x80

#define RA8875_MWCR0            0x40
#define RA8875_MWCR0_GFXMODE    0x00
#define RA8875_MWCR0_TXTMODE    0x80

#define RA8875_CURH0            0x46
#define RA8875_CURH1            0x47
#define RA8875_CURV0            0x48
#define RA8875_CURV1            0x49

#define RA8875_P1CR             0x8A
#define RA8875_P1CR_ENABLE      0x80
#define RA8875_P1CR_DISABLE     0x00
#define RA8875_P1CR_CLKOUT      0x10
#define RA8875_P1CR_PWMOUT      0x00

#define RA8875_P1DCR            0x8B

#define RA8875_P2CR             0x8C
#define RA8875_P2CR_ENABLE      0x80
#define RA8875_P2CR_DISABLE     0x00
#define RA8875_P2CR_CLKOUT      0x10
#define RA8875_P2CR_PWMOUT      0x00

#define RA8875_P2DCR            0x8D

#define RA8875_PWM_CLK_DIV1     0x00
#define RA8875_PWM_CLK_DIV2     0x01
#define RA8875_PWM_CLK_DIV4     0x02
#define RA8875_PWM_CLK_DIV8     0x03
#define RA8875_PWM_CLK_DIV16    0x04
#define RA8875_PWM_CLK_DIV32    0x05
#define RA8875_PWM_CLK_DIV64    0x06
#define RA8875_PWM_CLK_DIV128   0x07
#define RA8875_PWM_CLK_DIV256   0x08
#define RA8875_PWM_CLK_DIV512   0x09
#define RA8875_PWM_CLK_DIV1024  0x0A
#define RA8875_PWM_CLK_DIV2048  0x0B
#define RA8875_PWM_CLK_DIV4096  0x0C
#define RA8875_PWM_CLK_DIV8192  0x0D
#define RA8875_PWM_CLK_DIV16384 0x0E
#define RA8875_PWM_CLK_DIV32768 0x0F

#define RA8875_TPCR0                  0x70
#define RA8875_TPCR0_ENABLE           0x80
#define RA8875_TPCR0_DISABLE          0x00
#define RA8875_TPCR0_WAIT_512CLK      0x00
#define RA8875_TPCR0_WAIT_1024CLK     0x10
#define RA8875_TPCR0_WAIT_2048CLK     0x20
#define RA8875_TPCR0_WAIT_4096CLK     0x30
#define RA8875_TPCR0_WAIT_8192CLK     0x40
#define RA8875_TPCR0_WAIT_16384CLK    0x50
#define RA8875_TPCR0_WAIT_32768CLK    0x60
#define RA8875_TPCR0_WAIT_65536CLK    0x70
#define RA8875_TPCR0_WAKEENABLE       0x08
#define RA8875_TPCR0_WAKEDISABLE      0x00
#define RA8875_TPCR0_ADCCLK_DIV1      0x00
#define RA8875_TPCR0_ADCCLK_DIV2      0x01
#define RA8875_TPCR0_ADCCLK_DIV4      0x02
#define RA8875_TPCR0_ADCCLK_DIV8      0x03
#define RA8875_TPCR0_ADCCLK_DIV16     0x04
#define RA8875_TPCR0_ADCCLK_DIV32     0x05
#define RA8875_TPCR0_ADCCLK_DIV64     0x06
#define RA8875_TPCR0_ADCCLK_DIV128    0x07

#define RA8875_TPCR1            0x71
#define RA8875_TPCR1_AUTO       0x00
#define RA8875_TPCR1_MANUAL     0x40
#define RA8875_TPCR1_VREFINT    0x00
#define RA8875_TPCR1_VREFEXT    0x20
#define RA8875_TPCR1_DEBOUNCE   0x04
#define RA8875_TPCR1_NODEBOUNCE 0x00
#define RA8875_TPCR1_IDLE       0x00
#define RA8875_TPCR1_WAIT       0x01
#define RA8875_TPCR1_LATCHX     0x02
#define RA8875_TPCR1_LATCHY     0x03

#define RA8875_TPXH             0x72
#define RA8875_TPYH             0x73
#define RA8875_TPXYL            0x74

#define RA8875_INTC1            0xF0
#define RA8875_INTC1_KEY        0x10
#define RA8875_INTC1_DMA        0x08
#define RA8875_INTC1_TP         0x04
#define RA8875_INTC1_BTE        0x02

#define RA8875_INTC2            0xF1
#define RA8875_INTC2_KEY        0x10
#define RA8875_INTC2_DMA        0x08
#define RA8875_INTC2_TP         0x04
#define RA8875_INTC2_BTE        0x02


typedef struct{
	alt_16 WIDTH, HEIGHT;   			// This is the 'raw' display w/h - never changes
	alt_16 _width, _height, 			// Display w/h as modified by current rotation
	cursor_x, cursor_y;					// x, y coordinates
	alt_u16 textcolor, textbgcolor;		// For use with drawChar() text
	alt_u8 _textScale, rotation;
	bool wrap;
	enum RA8875sizes _size;

} Adafruit_LCD;

void Adafruit_LCD_construct(Adafruit_LCD *lcd, alt_16 w, alt_16 h );

void    softReset(void);
void 	PLLinit(void);
void	initialize(Adafruit_LCD *lcd);
void    displayOn(bool on);
void    sleep(bool sleep);
alt_u16 width(Adafruit_LCD *lcd);
alt_u16 height(Adafruit_LCD *lcd);
void 	delay(alt_u32 ms);

/* Text Mode */
void 	textMode(void);
void 	textSetCursor(alt_u16 x, alt_u16 y);
void 	textColor(alt_u16 foreColor, alt_u16 bgColor);
void 	textEnlarge(Adafruit_LCD *lcd, alt_u8 scale);
void 	textWrite(Adafruit_LCD *lcd, const char* buffer, alt_u16 len);

/* Graphics Mode */
void 	graphicsMode(void);
bool 	waitPoll(alt_u8 regname, alt_u8 waitflag);
void 	setXY(alt_u16 x, alt_u16 y);
void 	fillRectVoid(void);

/* Adafruit_RA8875 functions */
void 	drawChar(Adafruit_LCD *lcd, alt_16 x, alt_16 y, unsigned char c, alt_u16 color, alt_u16 bg, alt_u8 size);
void    drawPixel(alt_16 x, alt_16 y, alt_u16 color);
void 	drawLine(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_u16 color);
void    drawFastVLine(alt_16 x, alt_16 y, alt_16 h, alt_u16 color);
void    drawFastHLine(alt_16 x, alt_16 y, alt_16 w, alt_u16 color);
void 	pushPixels(alt_u32 num, alt_u16 p);

/* HW accelerated wrapper functions (override Adafruit_GFX prototypes) */
void    fillScreen(Adafruit_LCD *lcd, alt_u16 color);
void    drawRect(alt_16 x, alt_16 y, alt_16 w, alt_16 h, alt_u16 color);
void    fillRect(alt_16 x, alt_16 y, alt_16 w, alt_16 h, alt_u16 color);
void    drawCircle(alt_16 x0, alt_16 y0, alt_16 r, alt_u16 color);
void    fillCircle(alt_16 x0, alt_16 y0, alt_16 r, alt_u16 color);
void    drawTriangle(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_16 x2, alt_16 y2, alt_u16 color);
void    fillTriangle(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_16 x2, alt_16 y2, alt_u16 color);
void    drawEllipse(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u16 color);
void    fillEllipse(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u16 color);
void    drawCurve(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u8 curvePart, alt_u16 color);
void    fillCurve(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u8 curvePart, alt_u16 color);

/* Custom */
void 	drawScreenBorder(Adafruit_LCD *lcd);
void 	drawNotificationBorder(Adafruit_LCD *lcd);

/* GFX Helper Functions */
void 	circleHelper(alt_16 x0, alt_16 y0, alt_16 r, alt_u16 color, bool filled);
void 	rectHelper  (alt_16 x, alt_16 y, alt_16 w, alt_16 h, alt_u16 color, bool filled);
void 	triangleHelper(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_16 x2, alt_16 y2, alt_u16 color, bool filled);
void 	ellipseHelper(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u16 color, bool filled);
void 	curveHelper(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u8 curvePart, alt_u16 color, bool filled);

/* Adafruit GFX/RA8875 shared functions */
void 	fillCircleHelper(alt_16 x0, alt_16 y0, alt_16 r, alt_u8 cornername, alt_16 delta, alt_u16 color);
void 	setTextWrap(Adafruit_LCD *lcd, bool w);
alt_u8	getRotation(Adafruit_LCD *lcd);
void 	setRotation(Adafruit_LCD *lcd, alt_u8 r);

/* Backlight */
void    GPIOX(bool on);
void    PWM1config(bool on, alt_u8 clock);
void    PWM2config(bool on, alt_u8 clock);
void    PWM1out(alt_u8 p);
void    PWM2out(alt_u8 p);

/* Touch Screen - Basic */
void    touchEnable(bool on);
bool 	touched(void);
bool 	touchRead(alt_u16 *x, alt_u16 *y);

/* Display On/Off Functions */
void 	displayOn(bool on);
void 	sleep(bool sleep);

/* Low-Level Functions */
void  	writeData(alt_u8 data);
void  	writeReg(alt_u8 reg, alt_u8 val);
alt_u8  readReg(alt_u8 reg);
alt_u8  readData(void);
void  	writeCommand(alt_u8 d);
alt_u8 	readStatus(void);
void	transferWrite(alt_u8 data);
alt_u8	transferRead(void);

#endif // _ADAFRUIT_LCD_H
