/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * This file has been copied from the library for the Adafruit RA8875 TFT
 * display driver board and adapted to work with the Altera DE2.
 * This file is written to power and write to a 800 x 480 pixel
 * resolution TFT display.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#include "Adafruit_LCD.h"
#include "system.h"
#include <stdlib.h>
#include <stdbool.h>
#include "altera_avalon_spi_regs.h"
#include "altera_avalon_spi.h"
#include "../lcd/glcdfont.c"

#define border_offset 20
/**************************************************************************/
/*!
      Initializes the characteristics of the TFT LCD display.
      Not all struct members are needed depending on the function
      used to write text to the display.
      The following variables are needed only if drawChar(...)
      is ever used:
      	  cursor_y
      	  cursor_x
      	  textsize
      	  textcolor
      	  textbgcolor

      @args lcd[in]   	Adafruit_LCD
      @args w[in]     	The width of the LCD display (800)
	  @args h[in]     	The height of the LCD display (480)

 */
/**************************************************************************/
void Adafruit_LCD_construct(Adafruit_LCD *lcd, alt_16 w, alt_16 h ){

	lcd->WIDTH 		= w;
	lcd->HEIGHT 	= h;
	lcd->_width		= w;
	lcd->_height	= h;
	lcd->rotation	= 0;
//	lcd->cursor_y	= lcd->cursor_x    = 0;
//	lcd->textcolor	= lcd->textbgcolor = 0xFFFF;
	lcd->wrap		= true;
}

/**************************************************************************/
/*!
      Performs a SW-based reset of the RA8875
 */
/**************************************************************************/
void softReset(void) {
	writeCommand(RA8875_PWRR);
	writeData(RA8875_PWRR_SOFTRESET);
	writeData(RA8875_PWRR_NORMAL);
	delay(1);
}
/**************************************************************************/
/*!
      Initialise the PLL
 */
/**************************************************************************/
void PLLinit(void) {
	writeReg(RA8875_PLLC1, RA8875_PLLC1_PLLDIV1 + 10);
	delay(1);
	writeReg(RA8875_PLLC2, RA8875_PLLC2_DIV4);
	delay(1);
}


/**************************************************************************/
/*!
      Initialises the driver IC (clock setup, etc.)
 */
/**************************************************************************/
void initialize(Adafruit_LCD *lcd) {
	PLLinit();
	writeReg(RA8875_SYSR, RA8875_SYSR_8BPP | RA8875_SYSR_MCU8);

	/* Timing values */
	alt_u8 pixclk;
	alt_u8 hsync_start;
	alt_u8 hsync_pw;
	alt_u8 hsync_finetune;
	alt_u8 hsync_nondisp;
	alt_u8 vsync_pw;
	alt_u16 vsync_nondisp;
	alt_u16 vsync_start;

	pixclk          = RA8875_PCSR_PDATL | RA8875_PCSR_2CLK;
	hsync_nondisp   = 26;
	hsync_start     = 32;
	hsync_pw        = 96;
	hsync_finetune  = 0;
	vsync_nondisp   = 32;
	vsync_start     = 23;
	vsync_pw        = 2;

	writeReg(RA8875_PCSR, pixclk);
	delay(1);

	/* Horizontal settings registers */
	writeReg(RA8875_HDWR, ((lcd->_width) / 8) - 1);                          // H width: (HDWR + 1) * 8 = 480
	writeReg(RA8875_HNDFTR, RA8875_HNDFTR_DE_HIGH + hsync_finetune);
	writeReg(RA8875_HNDR, (hsync_nondisp - hsync_finetune - 2)/8);    // H non-display: HNDR * 8 + HNDFTR + 2 = 10
	writeReg(RA8875_HSTR, hsync_start/8 - 1);                         // Hsync start: (HSTR + 1)*8
	writeReg(RA8875_HPWR, RA8875_HPWR_LOW + (hsync_pw/8 - 1));        // HSync pulse width = (HPWR+1) * 8

	/* Vertical settings registers */
	writeReg(RA8875_VDHR0, (alt_u16)((lcd->_height) - 1) & 0xFF);
	writeReg(RA8875_VDHR1, (alt_u16)((lcd->_height) - 1) >> 8);
	writeReg(RA8875_VNDR0, vsync_nondisp-1);                          // V non-display period = VNDR + 1
	writeReg(RA8875_VNDR1, vsync_nondisp >> 8);
	writeReg(RA8875_VSTR0, vsync_start-1);                            // Vsync start position = VSTR + 1
	writeReg(RA8875_VSTR1, vsync_start >> 8);
	writeReg(RA8875_VPWR, RA8875_VPWR_LOW + vsync_pw - 1);            // Vsync pulse width = VPWR + 1

	/* Set active window X */
	writeReg(RA8875_HSAW0, 0);                                        // horizontal start point
	writeReg(RA8875_HSAW1, 0);
	writeReg(RA8875_HEAW0, (alt_u16)((lcd->_width) - 1) & 0xFF);            // horizontal end point
	writeReg(RA8875_HEAW1, (alt_u16)((lcd->_width) - 1) >> 8);

	/* Set active window Y */
	writeReg(RA8875_VSAW0, 0);                                        // vertical start point
	writeReg(RA8875_VSAW1, 0);
	writeReg(RA8875_VEAW0, (alt_u16)((lcd->_height) - 1) & 0xFF);           // horizontal end point
	writeReg(RA8875_VEAW1, (alt_u16)((lcd->_height) - 1) >> 8);

	/* ToDo: Setup touch panel? */

	/* Clear the entire window */
	writeReg(RA8875_MCLR, RA8875_MCLR_START | RA8875_MCLR_FULL);
	delay(500);
}

/**************************************************************************/
/*!
      Returns the display width in pixels

      @returns  The 1-based display width in pixels
 */
/**************************************************************************/
alt_u16 width(Adafruit_LCD *lcd) { return lcd->_width; }

/**************************************************************************/
/*!
      Returns the display height in pixels

      @returns  The 1-based display height in pixels
 */
/**************************************************************************/
alt_u16 height(Adafruit_LCD *lcd) { return lcd->_height; }

/**************************************************************************/
/*!
      Delays operation for the number of time input

      @args ms[in] Delay time in milliseconds
 */
/**************************************************************************/
void delay(alt_u32 ms){
    alt_u32 currentTime;

    alt_u32 StartTime = OSTimeGet()/(OS_TICKS_PER_SEC/1000);
    currentTime = OSTimeGet()/(OS_TICKS_PER_SEC/1000);

    while((currentTime - StartTime) < ms)
    {
    	currentTime = OSTimeGet()/(OS_TICKS_PER_SEC/1000);
    }
}
/**************************************************************************/


//TODO: Text Mode
/************************* Text Mode ***********************************/

/**************************************************************************/
/*!
      Sets the display in text mode (as opposed to graphics mode)
 */
/**************************************************************************/
void textMode(void){
	/* Set text mode */
	writeCommand(RA8875_MWCR0);
	alt_u8 temp = readData();
	temp |= RA8875_MWCR0_TXTMODE; // Set bit 7
	writeData(temp);

	/* Select the internal (ROM) font */
	writeCommand(0x21);
	temp = readData();
	temp &= ~((1<<7) | (1<<5)); // Clear bits 7 and 5
	writeData(temp);
}

/**************************************************************************/
/*!
      Sets the display in text mode (as opposed to graphics mode)

      @args x[in] The x position of the cursor (in pixels, 0..1023)
      @args y[in] The y position of the cursor (in pixels, 0..511)
 */
/**************************************************************************/
void textSetCursor(alt_u16 x, alt_u16 y){
	/* Set cursor location */
	writeCommand(0x2A);
	writeData(x & 0xFF);
	writeCommand(0x2B);
	writeData(x >> 8);
	writeCommand(0x2C);
	writeData(y & 0xFF);
	writeCommand(0x2D);
	writeData(y >> 8);
}

/**************************************************************************/
/*!
      Sets the fore and background color when rendering text

      @args foreColor[in] The RGB565 color to use when rendering the text
      @args bgColor[in]   The RGB565 colot to use for the background
 */
/**************************************************************************/
void textColor(alt_u16 foreColor, alt_u16 bgColor){
	/* Set Fore Color */
	writeCommand(0x63);
	writeData((foreColor & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((foreColor & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((foreColor & 0x001f));

	/* Set Background Color */
	writeCommand(0x60);
	writeData((bgColor & 0xf800) >> 11);
	writeCommand(0x61);
	writeData((bgColor & 0x07e0) >> 5);
	writeCommand(0x62);
	writeData((bgColor & 0x001f));

	/* Clear transparency flag */
	writeCommand(0x22);
	alt_u8 temp = readData();
	temp &= ~(1<<6); // Clear bit 6
	writeData(temp);
}


/**************************************************************************/
/*!
      Sets the text enlarge settings, using one of the following values:

      0 = 1x zoom
      1 = 2x zoom
      2 = 3x zoom
      3 = 4x zoom

      @args scale[in]   The zoom factor (0..3 for 1-4x zoom)
 */
/**************************************************************************/
void textEnlarge(Adafruit_LCD *lcd, alt_u8 scale){
	if (scale > 3) scale = 3;

	/* Set font size flags */
	writeCommand(0x22);
	alt_u8 temp = readData();
	temp &= ~(0xF); // Clears bits 0..3
	temp |= scale << 2;
	temp |= scale;
	writeData(temp);

	lcd->_textScale = scale;
}

/**************************************************************************/
/*!
      Renders some text on the screen when in text mode

      @args buffer[in]    The buffer containing the characters to render
      @args len[in]       The size of the buffer in bytes
 */
/**************************************************************************/
void textWrite(Adafruit_LCD *lcd, const char* buffer, alt_u16 len){
	alt_u16 i;
	if (len == 0) len = strlen(buffer);
	writeCommand(RA8875_MRWC);
	for (i=0;i<len;i++){
		writeData(buffer[i]);

		// This delay is needed with textEnlarge(1) because
		// Teensy 3.X is much faster than Arduino Uno
		if (lcd->_textScale > 0) delay(1);
	}
}

//TODO : Graphics
/***************************** Graphics ***********************************/
/**************************************************************************/
/*!
      Sets the display in graphics mode (as opposed to text mode)
 */
/**************************************************************************/
void graphicsMode(void) {
	writeCommand(RA8875_MWCR0);
	alt_u8 temp = readData();
	temp &= ~RA8875_MWCR0_TXTMODE; // bit #7
	writeData(temp);
}
/**************************************************************************/
/*!
      Draws a char using added fonts (ex: glcdfont.c). Must be in
      graphics mode to write text to the screen with this method.
      Use this method to write text if a different font or larger
      text is needed. Otherwise, use textWrite().

      Note: This writes text to the screen slower than textWrite

      @args lcd[in]   	Adafruit_LCD
      @args x[in]     	The 0-based x location
	  @args y[in]     	The 0-base y location
      @args c[in]     	The char to write
      @args color[in]   Colour of the text
      @args bg[in]     	Backgound colour of the text
      @args size[in] 	Font size
 */
/**************************************************************************/
void drawChar(Adafruit_LCD *lcd, alt_16 x, alt_16 y, unsigned char c,
		alt_u16 color, alt_u16 bg, alt_u8 size) {

	if((x >= lcd->_width)            || // Clip right
			(y >= lcd->_height)      || // Clip bottom
			((x + 6 * size - 1) < 0) || // Clip left
			((y + 8 * size - 1) < 0))   // Clip top
		return;
	alt_8 i,j;

	for (i=0; i<6; i++ ) {
		alt_u8 line;
		if (i == 5)
			line = 0x0;
		else
			line = font[(c*5)+i];
		for (j = 0; j<8; j++) {
			if (line & 0x1) {
				if (size == 1) // default size
					drawPixel(x+i, y+j, color);
				else {  // big size
					fillRect(x+(i*size), y+(j*size), size, size, color);
				}
			} else if (bg != color) {
				if (size == 1) // default size
					drawPixel(x+i, y+j, bg);
				else {  // big size
					fillRect(x+i*size, y+j*size, size, size, bg);
				}
			}
			line >>= 1;
		}
	}
}


/**************************************************************************/
/*!
      Waits for screen to finish by polling the status!
 */
/**************************************************************************/
bool waitPoll(alt_u8 regname, alt_u8 waitflag) {
	/* Wait for the command to finish */
	while (1)
	{
		alt_u8 temp = readReg(regname);
		if (!(temp & waitflag))
			return true;
	}
	return false; // MEMEFIX: yeah i know, unreached! - add timeout?
}

/**************************************************************************/
/*!
      Sets the current X/Y position on the display before drawing

      @args x[in] The 0-based x location
      @args y[in] The 0-base y location
 */
/**************************************************************************/
void setXY(alt_u16 x, alt_u16 y) {
	writeReg(RA8875_CURH0, x);
	writeReg(RA8875_CURH1, x >> 8);
	writeReg(RA8875_CURV0, y);
	writeReg(RA8875_CURV1, y >> 8);
}

/**************************************************************************/
/*!
  	  K: Not quite sure what purpose this has *
 */
/**************************************************************************/
void fillRectVoid(void) {
	writeCommand(RA8875_DCR);
	writeData(RA8875_DCR_LINESQUTRI_STOP | RA8875_DCR_DRAWSQUARE);
	writeData(RA8875_DCR_LINESQUTRI_START | RA8875_DCR_FILL | RA8875_DCR_DRAWSQUARE);
}

/**************************************************************************/
/*!
      Draws a single pixel at the specified location

      @args x[in]     The 0-based x location
      @args y[in]     The 0-base y location
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawPixel(alt_16 x, alt_16 y, alt_u16 color)
{
	writeReg(RA8875_CURH0, x);
	writeReg(RA8875_CURH1, x >> 8);
	writeReg(RA8875_CURV0, y);
	writeReg(RA8875_CURV1, y >> 8);
	writeCommand(RA8875_MRWC);
	transferWrite(RA8875_DATAWRITE);
	transferWrite(color >> 8);
	transferWrite(color);
}
/**************************************************************************/
/*!
      Draws a HW accelerated line on the display

      @args x0[in]    The 0-based starting x location
      @args y0[in]    The 0-base starting y location
      @args x1[in]    The 0-based ending x location
      @args y1[in]    The 0-base ending y location
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawLine(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_u16 color)
{
	/* Set X */
	writeCommand(0x91);
	writeData(x0);
	writeCommand(0x92);
	writeData(x0 >> 8);

	/* Set Y */
	writeCommand(0x93);
	writeData(y0);
	writeCommand(0x94);
	writeData(y0 >> 8);

	/* Set X1 */
	writeCommand(0x95);
	writeData(x1);
	writeCommand(0x96);
	writeData((x1) >> 8);

	/* Set Y1 */
	writeCommand(0x97);
	writeData(y1);
	writeCommand(0x98);
	writeData((y1) >> 8);

	/* Set Color */
	writeCommand(0x63);
	writeData((color & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((color & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((color & 0x001f));

	/* Draw! */
	writeCommand(RA8875_DCR);
	writeData(0x80);

	/* Wait for the command to finish */
	waitPoll(RA8875_DCR, RA8875_DCR_LINESQUTRI_STATUS);
}

/**************************************************************************/
/*!

 */
/**************************************************************************/
void drawFastVLine(alt_16 x, alt_16 y, alt_16 h, alt_u16 color)
{
	drawLine(x, y, x, y+h, color);
}

/**************************************************************************/
/*!

 */
/**************************************************************************/
void drawFastHLine(alt_16 x, alt_16 y, alt_16 w, alt_u16 color)
{
	drawLine(x, y, x+w, y, color);
}

/**************************************************************************/
/*!
      HW accelerated function to push a chunk of raw pixel data

      @args num[in] The number of pixels to push
      @args p[in]   The pixel color to use
 */
/**************************************************************************/
void pushPixels(alt_u32 num, alt_u16 p) {
	transferWrite(RA8875_DATAWRITE);
	while (num--) {
		transferWrite(p >> 8);
		transferWrite(p);
	}
}
/**************************************************************************/
/*!
      Fills the screen with the spefied RGB565 color

      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void fillScreen(Adafruit_LCD *lcd, alt_u16 color)
{
	rectHelper(0, 0, (lcd->_width)-1, (lcd->_height)-1, color, true);
}


//TODO : Add in DrawShapes Here
/**************************************************************************/
/*!
      Draws a HW accelerated rectangle on the display

      @args x[in]     The 0-based x location of the top-right corner
      @args y[in]     The 0-based y location of the top-right corner
      @args w[in]     The rectangle width
      @args h[in]     The rectangle height
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawRect(alt_16 x, alt_16 y, alt_16 w, alt_16 h, alt_u16 color)
{
	rectHelper(x, y, x+w, y+h, color, false);
}
/**************************************************************************/
/*!
      Draws a HW accelerated filled rectangle on the display

      @args x[in]     The 0-based x location of the top-right corner
      @args y[in]     The 0-based y location of the top-right corner
      @args w[in]     The rectangle width
      @args h[in]     The rectangle height
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void fillRect(alt_16 x, alt_16 y, alt_16 w, alt_16 h, alt_u16 color){
	rectHelper(x, y, x+w, y+h, color, true);
}

/**************************************************************************/
/*!
      Draws a HW accelerated circle on the display

      @args x[in]     The 0-based x location of the center of the circle
      @args y[in]     The 0-based y location of the center of the circle
      @args w[in]     The circle's radius
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawCircle(alt_16 x0, alt_16 y0, alt_16 r, alt_u16 color){
	circleHelper(x0, y0, r, color, false);
}

/**************************************************************************/
/*!
      Draws a HW accelerated filled circle on the display

      @args x[in]     The 0-based x location of the center of the circle
      @args y[in]     The 0-based y location of the center of the circle
      @args w[in]     The circle's radius
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void fillCircle(alt_16 x0, alt_16 y0, alt_16 r, alt_u16 color){
	circleHelper(x0, y0, r, color, true);
}

/**************************************************************************/
/*!
      Draws a HW accelerated triangle on the display

      @args x0[in]    The 0-based x location of point 0 on the triangle
      @args y0[in]    The 0-based y location of point 0 on the triangle
      @args x1[in]    The 0-based x location of point 1 on the triangle
      @args y1[in]    The 0-based y location of point 1 on the triangle
      @args x2[in]    The 0-based x location of point 2 on the triangle
      @args y2[in]    The 0-based y location of point 2 on the triangle
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawTriangle(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_16 x2, alt_16 y2, alt_u16 color){
	triangleHelper(x0, y0, x1, y1, x2, y2, color, false);
}

/**************************************************************************/
/*!
      Draws a HW accelerated filled triangle on the display

      @args x0[in]    The 0-based x location of point 0 on the triangle
      @args y0[in]    The 0-based y location of point 0 on the triangle
      @args x1[in]    The 0-based x location of point 1 on the triangle
      @args y1[in]    The 0-based y location of point 1 on the triangle
      @args x2[in]    The 0-based x location of point 2 on the triangle
      @args y2[in]    The 0-based y location of point 2 on the triangle
      @args color[in] The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void fillTriangle(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_16 x2, alt_16 y2, alt_u16 color){
	triangleHelper(x0, y0, x1, y1, x2, y2, color, true);
}

/**************************************************************************/
/*!
      Draws a HW accelerated ellipse on the display

      @args xCenter[in]   The 0-based x location of the ellipse's center
      @args yCenter[in]   The 0-based y location of the ellipse's center
      @args longAxis[in]  The size in pixels of the ellipse's long axis
      @args shortAxis[in] The size in pixels of the ellipse's short axis
      @args color[in]     The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawEllipse(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u16 color){
	ellipseHelper(xCenter, yCenter, longAxis, shortAxis, color, false);
}

/**************************************************************************/
/*!
      Draws a HW accelerated filled ellipse on the display

      @args xCenter[in]   The 0-based x location of the ellipse's center
      @args yCenter[in]   The 0-based y location of the ellipse's center
      @args longAxis[in]  The size in pixels of the ellipse's long axis
      @args shortAxis[in] The size in pixels of the ellipse's short axis
      @args color[in]     The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void fillEllipse(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u16 color){
	ellipseHelper(xCenter, yCenter, longAxis, shortAxis, color, true);
}

/**************************************************************************/
/*!
      Draws a HW accelerated curve on the display

      @args xCenter[in]   The 0-based x location of the ellipse's center
      @args yCenter[in]   The 0-based y location of the ellipse's center
      @args longAxis[in]  The size in pixels of the ellipse's long axis
      @args shortAxis[in] The size in pixels of the ellipse's short axis
      @args curvePart[in] The corner to draw, where in clock-wise motion:
                            0 = 180-270�
                            1 = 270-0�
                            2 = 0-90�
                            3 = 90-180�
      @args color[in]     The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void drawCurve(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u8 curvePart, alt_u16 color){
	curveHelper(xCenter, yCenter, longAxis, shortAxis, curvePart, color, false);
}

/**************************************************************************/
/*!
      Draws a HW accelerated filled curve on the display

      @args xCenter[in]   The 0-based x location of the ellipse's center
      @args yCenter[in]   The 0-based y location of the ellipse's center
      @args longAxis[in]  The size in pixels of the ellipse's long axis
      @args shortAxis[in] The size in pixels of the ellipse's short axis
      @args curvePart[in] The corner to draw, where in clock-wise motion:
                            0 = 180-270�
                            1 = 270-0�
                            2 = 0-90�
                            3 = 90-180�
      @args color[in]     The RGB565 color to use when drawing the pixel
 */
/**************************************************************************/
void fillCurve(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u8 curvePart, alt_u16 color){
	curveHelper(xCenter, yCenter, longAxis, shortAxis, curvePart, color, true);
}

//TODO: Custom
/******************************* CUSTOM ***********************************/

/**************************************************************************/
/*!
      Draws a rectangular border 10 pixels wide of different colours.
      Distance away from screen edges are denoted by border_offset.

         @args lcd[in]     Adafruit_LCD
 */
/**************************************************************************/
void drawScreenBorder(Adafruit_LCD *lcd){
	int i;
	int border_thickness = 14;

	alt_u16 colours[7] = {RA8875_DARKPURPLE, RA8875_DARKBLUE, RA8875_INDIGO1, RA8875_INDIGO2,RA8875_ORCHID,RA8875_PINK, RA8875_LIGHTORANGE};
	for (i=0 ; i<border_thickness ; i+=2){
		rectHelper(border_offset+i, border_offset+i, lcd->_width-border_offset-i, lcd->_height-border_offset-i, colours[i/2], false);
	}
}
/**************************************************************************/
/*!
      Draws a two horizontal border lines 18 pixels wide of different colours.
      Used for task displaying notification contents.
      Distance away from screen edges are denoted by border_offset.

          @args lcd[in]     Adafruit_LCD

 */
/**************************************************************************/
void drawNotificationBorder(Adafruit_LCD *lcd){
	int i;
	int border_thickness = 18;
	alt_u16 colours[9] = {RA8875_BLUE,RA8875_RED,RA8875_INDIGO1,RA8875_ORANGE,RA8875_CYAN,RA8875_PINK,RA8875_LIGHTCYAN, RA8875_LIGHTORANGE, RA8875_GREEN};

	for (i=0 ; i<border_thickness ; i+=2){
		drawLine(border_offset, border_offset+i, lcd->_width-border_offset, border_offset+i, colours[i/2]);
		drawLine(border_offset, lcd->_height-border_offset-i, lcd->_width-border_offset, lcd->_height-border_offset-i, colours[i/2]);
	}
}


//TODO : Add in Shape Helpers
/************************** GFX Shape Helpers *****************************/

/**************************************************************************/
void circleHelper(alt_16 x0, alt_16 y0, alt_16 r, alt_u16 color, bool filled){
	/* Set X */
	writeCommand(0x99);
	writeData(x0);
	writeCommand(0x9a);
	writeData(x0 >> 8);

	/* Set Y */
	writeCommand(0x9b);
	writeData(y0);
	writeCommand(0x9c);
	writeData(y0 >> 8);

	/* Set Radius */
	writeCommand(0x9d);
	writeData(r);

	/* Set Color */
	writeCommand(0x63);
	writeData((color & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((color & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((color & 0x001f));

	/* Draw! */
	writeCommand(RA8875_DCR);
	if (filled)
	{
		writeData(RA8875_DCR_CIRCLE_START | RA8875_DCR_FILL);
	}
	else
	{
		writeData(RA8875_DCR_CIRCLE_START | RA8875_DCR_NOFILL);
	}

	/* Wait for the command to finish */
	waitPoll(RA8875_DCR, RA8875_DCR_CIRCLE_STATUS);
}

/**************************************************************************/
/*!
      Helper function for higher level rectangle drawing code
 */
/**************************************************************************/
void rectHelper(alt_16 x, alt_16 y, alt_16 w, alt_16 h, alt_u16 color, bool filled){
	/* Set X */
	writeCommand(0x91);
	writeData(x);
	writeCommand(0x92);
	writeData(x >> 8);

	/* Set Y */
	writeCommand(0x93);
	writeData(y);
	writeCommand(0x94);
	writeData(y >> 8);

	/* Set X1 */
	writeCommand(0x95);
	writeData(w);
	writeCommand(0x96);
	writeData((w) >> 8);

	/* Set Y1 */
	writeCommand(0x97);
	writeData(h);
	writeCommand(0x98);
	writeData((h) >> 8);

	/* Set Color */
	writeCommand(0x63);
	writeData((color & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((color & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((color & 0x001f));

	/* Draw! */
	writeCommand(RA8875_DCR);
	if (filled)
	{
		writeData(0xB0);
	}
	else
	{
		writeData(0x90);
	}

	/* Wait for the command to finish */
	waitPoll(RA8875_DCR, RA8875_DCR_LINESQUTRI_STATUS);
}

/**************************************************************************/
/*!
      Helper function for higher level triangle drawing code
 */
/**************************************************************************/
void triangleHelper(alt_16 x0, alt_16 y0, alt_16 x1, alt_16 y1, alt_16 x2, alt_16 y2, alt_u16 color, bool filled){
	/* Set Point 0 */
	writeCommand(0x91);
	writeData(x0);
	writeCommand(0x92);
	writeData(x0 >> 8);
	writeCommand(0x93);
	writeData(y0);
	writeCommand(0x94);
	writeData(y0 >> 8);

	/* Set Point 1 */
	writeCommand(0x95);
	writeData(x1);
	writeCommand(0x96);
	writeData(x1 >> 8);
	writeCommand(0x97);
	writeData(y1);
	writeCommand(0x98);
	writeData(y1 >> 8);

	/* Set Point 2 */
	writeCommand(0xA9);
	writeData(x2);
	writeCommand(0xAA);
	writeData(x2 >> 8);
	writeCommand(0xAB);
	writeData(y2);
	writeCommand(0xAC);
	writeData(y2 >> 8);

	/* Set Color */
	writeCommand(0x63);
	writeData((color & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((color & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((color & 0x001f));

	/* Draw! */
	writeCommand(RA8875_DCR);
	if (filled)
	{
		writeData(0xA1);
	}
	else
	{
		writeData(0x81);
	}

	/* Wait for the command to finish */
	waitPoll(RA8875_DCR, RA8875_DCR_LINESQUTRI_STATUS);
}

/**************************************************************************/
/*!
      Helper function for higher level ellipse drawing code
 */
/**************************************************************************/
void ellipseHelper(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u16 color, bool filled){
	/* Set Center Point */
	writeCommand(0xA5);
	writeData(xCenter);
	writeCommand(0xA6);
	writeData(xCenter >> 8);
	writeCommand(0xA7);
	writeData(yCenter);
	writeCommand(0xA8);
	writeData(yCenter >> 8);

	/* Set Long and Short Axis */
	writeCommand(0xA1);
	writeData(longAxis);
	writeCommand(0xA2);
	writeData(longAxis >> 8);
	writeCommand(0xA3);
	writeData(shortAxis);
	writeCommand(0xA4);
	writeData(shortAxis >> 8);

	/* Set Color */
	writeCommand(0x63);
	writeData((color & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((color & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((color & 0x001f));

	/* Draw! */
	writeCommand(0xA0);
	if (filled)
	{
		writeData(0xC0);
	}
	else
	{
		writeData(0x80);
	}

	/* Wait for the command to finish */
	waitPoll(RA8875_ELLIPSE, RA8875_ELLIPSE_STATUS);
}

/**************************************************************************/
/*!
      Helper function for higher level curve drawing code
 */
/**************************************************************************/
void curveHelper(alt_16 xCenter, alt_16 yCenter, alt_16 longAxis, alt_16 shortAxis, alt_u8 curvePart, alt_u16 color, bool filled){
	/* Set Center Point */
	writeCommand(0xA5);
	writeData(xCenter);
	writeCommand(0xA6);
	writeData(xCenter >> 8);
	writeCommand(0xA7);
	writeData(yCenter);
	writeCommand(0xA8);
	writeData(yCenter >> 8);

	/* Set Long and Short Axis */
	writeCommand(0xA1);
	writeData(longAxis);
	writeCommand(0xA2);
	writeData(longAxis >> 8);
	writeCommand(0xA3);
	writeData(shortAxis);
	writeCommand(0xA4);
	writeData(shortAxis >> 8);

	/* Set Color */
	writeCommand(0x63);
	writeData((color & 0xf800) >> 11);
	writeCommand(0x64);
	writeData((color & 0x07e0) >> 5);
	writeCommand(0x65);
	writeData((color & 0x001f));

	/* Draw! */
	writeCommand(0xA0);
	if (filled)
	{
		writeData(0xD0 | (curvePart & 0x03));
	}
	else
	{
		writeData(0x90 | (curvePart & 0x03));
	}

	/* Wait for the command to finish */
	waitPoll(RA8875_ELLIPSE, RA8875_ELLIPSE_STATUS);
}


//TODO : Adafruit GFX/RA8875 shared functions
/********************** Arduino_GFX Functions **************************/

/**************************************************************************/
/*!
      Sets the display in graphics mode (as opposed to text mode)
 */
/**************************************************************************/
// Used to do circles and roundrects
void  fillCircleHelper(alt_16 x0, alt_16 y0, alt_16 r, alt_u8 cornername, alt_16 delta, alt_u16 color) {

	alt_16 f     = 1 - r;
	alt_16 ddF_x = 1;
	alt_16 ddF_y = -2 * r;
	alt_16 x     = 0;
	alt_16 y     = r;

	while (x<y) {
		if (f >= 0) {
			y--;
			ddF_y += 2;
			f     += ddF_y;
		}
		x++;
		ddF_x += 2;
		f     += ddF_x;

		if (cornername & 0x1) {
			drawFastVLine(x0+x, y0-y, 2*y+1+delta, color);
			drawFastVLine(x0+y, y0-x, 2*x+1+delta, color);
		}
		if (cornername & 0x2) {
			drawFastVLine(x0-x, y0-y, 2*y+1+delta, color);
			drawFastVLine(x0-y, y0-x, 2*x+1+delta, color);
		}
	}
}

//void  setTextWrap(Adafruit_LCD *lcd, bool w) {
//	lcd->wrap = w;
//}

alt_u8  getRotation(Adafruit_LCD *lcd) {
	return lcd->rotation;
}

void  setRotation(Adafruit_LCD *lcd, alt_u8 x) {
	lcd->rotation = (x & 3);
	switch(lcd->rotation) {
	case 0:
	case 2:
		lcd->_width  = lcd->WIDTH;
		lcd->_height = lcd->HEIGHT;
		break;
	case 1:
	case 3:
		lcd->_width  = lcd->HEIGHT;
		lcd->_height = lcd->WIDTH;
		break;
	}
}

/******************************* Backlight ********************************/
/**************************************************************************/
/*!
 */
/**************************************************************************/
void GPIOX(bool on) {
	if (on)
		writeReg(RA8875_GPIOX, 1);
	else
		writeReg(RA8875_GPIOX, 0);
}

/**************************************************************************/
/*!
 */
/**************************************************************************/
void PWM1out(alt_u8 p) {
	writeReg(RA8875_P1DCR, p);
}

/**************************************************************************/
/*!
 */
/**************************************************************************/
void PWM2out(alt_u8 p) {
	writeReg(RA8875_P2DCR, p);
}

/**************************************************************************/
/*!
 */
/**************************************************************************/
void PWM1config(bool on, alt_u8 clock) {
	if (on) {
		writeReg(RA8875_P1CR, RA8875_P1CR_ENABLE | (clock & 0xF));
	} else {
		writeReg(RA8875_P1CR, RA8875_P1CR_DISABLE | (clock & 0xF));
	}
}

/**************************************************************************/
/*!
 */
/**************************************************************************/
void PWM2config(bool on, alt_u8 clock) {
	if (on) {
		writeReg(RA8875_P2CR, RA8875_P2CR_ENABLE | (clock & 0xF));
	} else {
		writeReg(RA8875_P2CR, RA8875_P2CR_DISABLE | (clock & 0xF));
	}
}


//TODO : Touch Screen Basics (Not using STMPE610)

/****************************** Touch Screen ******************************/
/**************************************************************************/
/*!
      Enables or disables the on-chip touch screen controller
 */
/**************************************************************************/
void touchEnable(bool on){
	if (on)
	{
		/* Enable Touch Panel (Reg 0x70) */
		writeReg(RA8875_TPCR0, RA8875_TPCR0_ENABLE        |
				RA8875_TPCR0_WAIT_4096CLK  |
				RA8875_TPCR0_WAKEDISABLE   |
				RA8875_TPCR0_ADCCLK_DIV4); // 10mhz max!
		/* Set Auto Mode      (Reg 0x71) */
		writeReg(RA8875_TPCR1, RA8875_TPCR1_AUTO    |
				// RA8875_TPCR1_VREFEXT |
				RA8875_TPCR1_DEBOUNCE);
		/* Enable TP INT */
		writeReg(RA8875_INTC1, readReg(RA8875_INTC1) | RA8875_INTC1_TP);
	}
	else
	{
		/* Disable TP INT */
		writeReg(RA8875_INTC1, readReg(RA8875_INTC1) & ~RA8875_INTC1_TP);
		/* Disable Touch Panel (Reg 0x70) */
		writeReg(RA8875_TPCR0, RA8875_TPCR0_DISABLE);
	}
}

/**************************************************************************/
/*!
      Checks if a touch event has occured

      @returns  True is a touch event has occured (reading it via
                touchRead() will clear the interrupt in memory)
 */
/**************************************************************************/
bool touched(void){
	if (readReg(RA8875_INTC2) & RA8875_INTC2_TP) return true;
	return false;
}

/**************************************************************************/
/*!
      Reads the last touch event

      @args x[out]  Pointer to the alt_u16 field to assign the raw X value
      @args y[out]  Pointer to the alt_u16 field to assign the raw Y value

      @note Calling this function will clear the touch panel interrupt on
            the RA8875, resetting the flag used by the 'touched' function
 */
/**************************************************************************/
bool touchRead(alt_u16 *x, alt_u16 *y){
	alt_u16 tx, ty;
	alt_u8 temp;

	tx = readReg(RA8875_TPXH);
	ty = readReg(RA8875_TPYH);
	temp = readReg(RA8875_TPXYL);
	tx <<= 2;
	ty <<= 2;
	tx |= temp & 0x03;        // get the bottom x bits
	ty |= (temp >> 2) & 0x03; // get the bottom y bits

	*x = tx;
	*y = ty;

	/* Clear TP INT Status */
	writeReg(RA8875_INTC2, RA8875_INTC2_TP);

	return true;
}


//TODO : Display ON/OFF

/**************************** Display ON/OFF ******************************/

/**************************************************************************/
/*!
      Turns the display on or off
 */
/**************************************************************************/
void displayOn(bool on){
	if (on)
		writeReg(RA8875_PWRR, RA8875_PWRR_NORMAL | RA8875_PWRR_DISPON);
	else
		writeReg(RA8875_PWRR, RA8875_PWRR_NORMAL | RA8875_PWRR_DISPOFF);
}

/**************************************************************************/
/*!
    Puts the display in sleep mode, or disables sleep mode if enabled
 */
/**************************************************************************/
void sleep(bool sleep){
	if (sleep)
		writeReg(RA8875_PWRR, RA8875_PWRR_DISPOFF | RA8875_PWRR_SLEEP);
	else
		writeReg(RA8875_PWRR, RA8875_PWRR_DISPOFF);
}

// TODO: Low Level
/**************************** Low Level ***********************************/

/**************************************************************************/
/**************************************************************************/
void  writeReg(alt_u8 reg, alt_u8 val){
	writeCommand(reg);
	writeData(val);
}

/**************************************************************************/
/**************************************************************************/
alt_u8  readReg(alt_u8 reg) {
	writeCommand(reg);
	return readData();
}

/**************************************************************************/
/**************************************************************************/
void  writeData(alt_u8 data){
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, ALTERA_AVALON_SPI_CONTROL_SSO_MSK);
	transferWrite(RA8875_DATAWRITE);
	transferWrite(data);
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, 0);
}

/**************************************************************************/
/**************************************************************************/
alt_u8  readData(void){
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, ALTERA_AVALON_SPI_CONTROL_SSO_MSK);
	transferWrite(RA8875_DATAREAD);
	alt_u8 x = transferRead();
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, 0);
	return x;
}

/**************************************************************************/
void  writeCommand(alt_u8 d) {
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, ALTERA_AVALON_SPI_CONTROL_SSO_MSK);

	transferWrite(RA8875_CMDWRITE);
	transferWrite(d);
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, 0);

}

/**************************************************************************/
alt_u8 readStatus(void) {
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, ALTERA_AVALON_SPI_CONTROL_SSO_MSK);
	transferWrite(RA8875_CMDREAD);
	alt_u8 x = transferRead();
	IOWR_ALTERA_AVALON_SPI_CONTROL(SPI_LCD_BASE, 0);
	return x;
}
/**************************************************************************/
/*!
		Transmits data to SPI
 */
/**************************************************************************/
void transferWrite(alt_u8 data) {

	alt_u32 status;

	/* Discard any stale data present in the RXDATA register, in case previous
	 * communication was interrupted and stale data was left behind. */
	IORD_ALTERA_AVALON_SPI_RXDATA(SPI_LCD_BASE);

	/* Transmit data */
	do{
		status = IORD_ALTERA_AVALON_SPI_STATUS(SPI_LCD_BASE);
	}
	while ((status & ALTERA_AVALON_SPI_STATUS_TRDY_MSK) == 0);

	if ((status & ALTERA_AVALON_SPI_STATUS_TRDY_MSK) != 0){
		IOWR_ALTERA_AVALON_SPI_TXDATA(SPI_LCD_BASE, data);
	};

	/* Wait until transmission has finished */
	while ((IORD_ALTERA_AVALON_SPI_STATUS(SPI_LCD_BASE) & ALTERA_AVALON_SPI_STATUS_TMT_MSK) == 0);

}
/**************************************************************************/
/*!
		Transmits 0x0 to SPI and receives/returns read data
 */
/**************************************************************************/
alt_u8 transferRead() {

	alt_u32 status;
	alt_u8   data;

	/* Discard any stale data present in the RXDATA register. */
	IORD_ALTERA_AVALON_SPI_RXDATA(SPI_LCD_BASE);

	/* Write 0x00 for read */
	IOWR_ALTERA_AVALON_SPI_TXDATA(SPI_LCD_BASE, 0x0);

	/* Transmit data */
	do{
		status = IORD_ALTERA_AVALON_SPI_STATUS(SPI_LCD_BASE);
	}
	while ((status & ALTERA_AVALON_SPI_STATUS_RRDY_MSK) == 0);

	data = IORD_ALTERA_AVALON_SPI_RXDATA(SPI_LCD_BASE);

	/* Wait until transmission has finished */
	while ((IORD_ALTERA_AVALON_SPI_STATUS(SPI_LCD_BASE) & ALTERA_AVALON_SPI_STATUS_TMT_MSK) == 0);

	return data;
}
/**************************************************************************/
