/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "error_handlers.h"

void mbox_pend_err(unsigned char err) {
	switch (err) {
	case OS_TIMEOUT:
		printf("OS_TIMEOUT: no message received within specified timeout\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a mailbox\n");
		break;
	case OS_ERR_PEND_ISR:
		printf("OS_ERR_PEND_ISR: pending on mailbox initiated within ISR\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void mbox_post_err(unsigned char err) {
	switch (err) {
	case OS_MBOX_FULL:
		printf("OS_MBOX_FULL: the mailbox already contains a message\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a mailbox\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	case OS_ERR_POST_NULL_PTR:
		printf("OS_ERR_POST_NULL_PTR: attempted to post a NULL pointer\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void mux_accept_err(unsigned char err) {
	switch (err) {
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a mutex\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	case OS_ERR_PEND_ISR:
		printf("OS_ERR_PEND_ISR: pending on mutex initiated within ISR\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void mux_create_err(unsigned char err) {
	switch (err) {
	case OS_ERR_CREATE_ISR:
		printf("OS_ERR_CREATE_ISR: mutex created within ISR\n");
		break;
	case OS_PRIO_EXIST:
		printf("OS_PRIO_EXIST: specified priority inheritance priority already exists\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	case OS_PRIO_INVALID:
		printf("OS_PRIO_INVALID: priority must be higher than OS_LOWEST_PRIO\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void mux_pend_err(unsigned char err) {
	switch (err) {
	case OS_TIMEOUT:
		printf("OS_TIMEOUT: unable to acquire lock within specified timeout\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a mutex\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	case OS_ERR_PEND_ISR:
		printf("OS_ERR_PEND_ISR: pending on mutex initiated within ISR\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void mux_post_err(unsigned char err) {
	switch (err) {
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a mutex\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	case OS_ERR_POST_ISR:
		printf("OS_ERR_PEND_ISR: post on mutex initiated within ISR\n");
		break;
	case OS_ERR_NOT_MUTEX_OWNER:
		printf("OS_ERR_NOT_MUTEX_OWNER: posting task is not mutex owner\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void q_pend_err(unsigned char err) {
	switch (err) {
	case OS_TIMEOUT:
		printf("OS_TIMEOUT: no message received within specified timeout\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a message queue\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	case OS_ERR_PEND_ISR:
		printf("OS_ERR_PEND_ISR: pending on queue initiated within ISR\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void q_post_err(unsigned char err) {
	switch (err) {
	case OS_Q_FULL:
		printf("OS_Q_FULL: message queue is full\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a message queue\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a null pointer\n");
		break;
	case OS_ERR_POST_NULL_PTR:
		printf("OS_ERR_POST_NULL_PTR: a NULL pointer was posted to the queue\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void q_query_err(unsigned char err) {
	switch (err) {
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a message queue\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a null pointer\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void sem_pend_err(unsigned char err) {
	switch (err) {
	case OS_TIMEOUT:
		printf("OS_TIMEOUT: semaphore was not signaled within specified timeout\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a semaphore\n");
		break;
	case OS_ERR_PEND_ISR:
		printf("OS_ERR_PEND_ISR: pending on semaphore initiated within ISR\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}

void sem_post_err(unsigned char err) {
	switch (err) {
	case OS_SEM_OVF:
		printf("OS_SEM_OVF: Semaphore count overflow\n");
		break;
	case OS_ERR_EVENT_TYPE:
		printf("OS_ERR_EVENT_TYPE: pevent is not pointing to a semaphore\n");
		break;
	case OS_ERR_PEVENT_NULL:
		printf("OS_ERR_PEVENT_NULL: pevent is a NULL pointer\n");
		break;
	default:
		printf("UNKNOWN: an unknown error has occurred\n");
		break;
	}
}
