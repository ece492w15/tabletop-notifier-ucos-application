/*
 * Copyright (C) 2015 Alexander Cheung, Krista Oribello, Michael Federau
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* filename: error_handlers.h
 *
 * The error handlers are simple utility functions which log the error messages
 * of system errors to stdout.
 */
#ifndef ERROR_HANDLERS_H
#define ERROR_HANDLERS_H

#include <ucos_ii.h>
#include <stdio.h>

void mbox_post_err(unsigned char err);
void mbox_pend_err(unsigned char err);

void mux_accept_err(unsigned char err);
void mux_create_err(unsigned char err);
void mux_pend_err(unsigned char err);
void mux_post_err(unsigned char err);

void q_pend_err(unsigned char err);
void q_post_err(unsigned char err);
void q_query_err(unsigned char err);

void sem_pend_err(unsigned char err);
void sem_post_err(unsigned char err);

#endif
